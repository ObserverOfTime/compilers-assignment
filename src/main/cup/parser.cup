/** Imports */
import gr.hua.dit.it21685.compilers.ast.*;
import gr.hua.dit.it21685.compilers.type.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Identifiers */
terminal String IDENTIFIER;

/** Literals */
terminal Character  CHAR_LITERAL;
terminal Float      FLOAT_LITERAL;
terminal String     STRING_LITERAL;
terminal Integer    INTEGER_LITERAL;
terminal Boolean    BOOLEAN_LITERAL;

/** Keywords */
terminal IF, ELSE;
terminal DOWN_TO, STEP, UNTIL;
terminal FOR, DO, WHILE;
terminal FUN, VAL, VAR;
terminal IN, NOT_IN;
terminal RETURN, BREAK, CONTINUE;
terminal BOOLEAN, CHAR, FLOAT, INT, STRING, UNIT;

/** Operators & separators */
terminal ADD, SUB;
terminal UPLUS, UMINUS;
terminal MUL, DIV, MOD;
terminal INCR, DECR;
terminal PRE_INCR, PRE_DECR;
terminal RANGE;
terminal NOT, AND, OR;
terminal EQ, NEQ;
terminal ID_EQ, ID_NEQ;
terminal LT, GT, LTE, GTE;
terminal ASSIGN,
         ADD_ASSIGN,
         SUB_ASSIGN,
         MUL_ASSIGN,
         DIV_ASSIGN,
         MOD_ASSIGN;
terminal LCURL, RCURL;
terminal LPAREN, RPAREN;
terminal COMMA, COLON, SEMI;

/** Non-terminals */
nonterminal Type Type;
nonterminal PropKind PropKind;
nonterminal Argument Argument;
nonterminal List<Argument> ArgumentList;
nonterminal Arguments Arguments;
nonterminal PostfixExpression PostfixExpression;
nonterminal PrefixExpression PrefixExpression;
nonterminal MultiplicativeExpression MultiplicativeExpression;
nonterminal AdditiveExpression AdditiveExpression;
nonterminal RangeExpression RangeExpression;
nonterminal ContainingExpression ContainingExpression;
nonterminal ComparisonExpression ComparisonExpression;
nonterminal EqualityExpression EqualityExpression;
nonterminal ConjunctionExpression ConjunctionExpression;
nonterminal DisjunctionExpression DisjunctionExpression;
nonterminal IdentifierExpression IdentifierExpression;
nonterminal LiteralExpression LiteralExpression;
nonterminal ParenExpression ParenExpression;
nonterminal Expression Expression;
nonterminal PropertyDeclaration PropertyDeclaration;
nonterminal Variable Variable;
nonterminal Parameter Parameter;
nonterminal List<Parameter> ParameterList;
nonterminal FunctionParameters FunctionParameters;
nonterminal FunctionDeclaration FunctionDeclaration;
nonterminal Statement Statement;
nonterminal BlockStatement BlockStatement;
nonterminal List<Statement> StatementList;
nonterminal Declaration Declaration;
nonterminal List<Declaration> DeclarationList;
nonterminal CompilationUnit CompilationUnit;

/** Precedences */
precedence nonassoc ASSIGN,
                    ADD_ASSIGN,
                    SUB_ASSIGN,
                    MUL_ASSIGN,
                    DIV_ASSIGN,
                    MOD_ASSIGN;
precedence left OR;
precedence left AND;
precedence nonassoc EQ, NEQ,
                    ID_EQ, ID_NEQ;
precedence nonassoc LT, GT, LTE, GTE;
precedence left IN, NOT_IN;
precedence left RANGE, DOWN_TO, UNTIL;
precedence nonassoc STEP;
precedence left ADD, SUB;
precedence left MUL, DIV, MOD;
precedence left COLON;
precedence right NOT,
                 UPLUS, UMINUS,
                 PRE_INCR, PRE_DECR;
precedence left INCR, DECR;
precedence left LPAREN;
precedence left ELSE;

/** Rules */
start with CompilationUnit;

CompilationUnit ::= DeclarationList:dl {:
    RESULT = new CompilationUnit(dl);
    RESULT.setPosition(dlleft, dlright);
:}                | {:
    RESULT = new CompilationUnit();
:}                ;

DeclarationList ::= Declaration:d {:
    RESULT = Collections.singletonList(d);
:}                | DeclarationList:dl Declaration:d {:
    RESULT = new ArrayList(dl);
    RESULT.add(d);
:}                ;

Declaration ::= FunctionDeclaration:d {:
    RESULT = d;
    d.setPosition(dleft, dright);
:}            | PropertyDeclaration:d {:
    RESULT = d;
    d.setPosition(dleft, dright);
:}            ;

Statement ::= Declaration:d {:
    RESULT = d;
    d.setPosition(dleft, dright);
:}          | DO:d Statement:s WHILE LPAREN Expression:e RPAREN SEMI {:
    RESULT = new DoWhileStatement(s, e);
    RESULT.setPosition(dleft, dright);
:}          | WHILE:w LPAREN Expression:e RPAREN Statement:s {:
    RESULT = new WhileStatement(e, s);
    RESULT.setPosition(wleft, wright);
:}          | FOR:f LPAREN Variable:v IN RangeExpression:e RPAREN Statement:s {:
    RESULT = new ForStatement(v, e, s);
    RESULT.setPosition(fleft, fright);
:}          | IF:i LPAREN Expression:e RPAREN Statement:s {:
    RESULT = new IfStatement(e, s);
    RESULT.setPosition(ileft, iright);
:}          | IF:i LPAREN Expression:e RPAREN Statement:s1 ELSE Statement:s2 {:
    RESULT = new IfElseStatement(e, s1, s2);
    RESULT.setPosition(ileft, iright);
:}          | IDENTIFIER:i ASSIGN Expression:e SEMI {:
    RESULT = new AssignStatement(i, AssignmentOperator.ASSIGN, e);
    RESULT.setPosition(ileft, iright);
:}          | IDENTIFIER:i ADD_ASSIGN Expression:e SEMI {:
    RESULT = new AssignStatement(i, AssignmentOperator.ADD_ASSIGN, e);
    RESULT.setPosition(ileft, iright);
:}          | IDENTIFIER:i SUB_ASSIGN Expression:e SEMI {:
    RESULT = new AssignStatement(i, AssignmentOperator.SUB_ASSIGN, e);
    RESULT.setPosition(ileft, iright);
:}          | IDENTIFIER:i MUL_ASSIGN Expression:e SEMI {:
    RESULT = new AssignStatement(i, AssignmentOperator.MUL_ASSIGN, e);
    RESULT.setPosition(ileft, iright);
:}          | IDENTIFIER:i DIV_ASSIGN Expression:e SEMI {:
    RESULT = new AssignStatement(i, AssignmentOperator.DIV_ASSIGN, e);
    RESULT.setPosition(ileft, iright);
:}          | IDENTIFIER:i MOD_ASSIGN Expression:e SEMI {:
    RESULT = new AssignStatement(i, AssignmentOperator.MOD_ASSIGN, e);
    RESULT.setPosition(ileft, iright);
:}          | Expression:e SEMI {:
    RESULT = new ExpressionStatement(e);
    RESULT.setPosition(eleft, eright);
:}          | BlockStatement:s {:
    RESULT = s;
    RESULT.setPosition(sleft, sright);
:}          | RETURN:r SEMI {:
    RESULT = new ReturnStatement();
    RESULT.setPosition(rleft, rright);
:}          | RETURN:r Expression:e SEMI {:
    RESULT = new ReturnStatement(e);
    RESULT.setPosition(rleft, rright);
:}          | CONTINUE:c SEMI {:
    RESULT = new ContinueStatement();
    RESULT.setPosition(cleft, cright);
:}          | BREAK:b SEMI {:
    RESULT = new BreakStatement();
    RESULT.setPosition(bleft, bright);
:}          ;

BlockStatement ::= LCURL:l RCURL {:
    RESULT = new BlockStatement();
    RESULT.setPosition(lleft, lright);
:}               | LCURL:l StatementList:sl RCURL {:
    RESULT = new BlockStatement(sl);
    RESULT.setPosition(lleft, lright);
:}               ;

StatementList ::= Statement:s {:
    RESULT = Collections.singletonList(s);
:}              | StatementList:sl Statement:s {:
    RESULT = new ArrayList(sl);
    RESULT.add(s);
:}              ;

PropertyDeclaration ::= PropKind:k Variable:v SEMI {:
    RESULT = new PropertyDeclaration(k, v);
    RESULT.setPosition(kleft, kright);
:}                    | PropKind:k Variable:v ASSIGN Expression:e SEMI {:
    RESULT = new PropertyDeclaration(k, v, e);
    RESULT.setPosition(kleft, kright);
:}                    ;

FunctionDeclaration ::= FUN:f IDENTIFIER:i FunctionParameters:p
                        BlockStatement:s {:
    RESULT = new FunctionDeclaration(i, p, s);
    RESULT.setPosition(fleft, fright);
:}                    | FUN:f IDENTIFIER:i FunctionParameters:p
                        COLON Type:t BlockStatement:s {:
    RESULT = new FunctionDeclaration(i, p, t, s);
    RESULT.setPosition(fleft, fright);
:}                    ;

FunctionParameters ::= LPAREN:l RPAREN {:
    RESULT = new FunctionParameters();
    RESULT.setPosition(lleft, lright);
:}                   | LPAREN:l ParameterList:pl RPAREN {:
    RESULT = new FunctionParameters(pl);
    RESULT.setPosition(lleft, lright);
:}                   ;

ParameterList ::= Parameter:p {:
    RESULT = Collections.singletonList(p);
:}              | ParameterList:pl COMMA Parameter:p {:
    RESULT = new ArrayList(pl);
    RESULT.add(p);
:}              ;

Parameter ::= IDENTIFIER:i COLON Type:t {:
    RESULT = new Parameter(i, t);
    RESULT.setPosition(ileft, iright);
:}          ;

Variable ::= IDENTIFIER:i {:
    RESULT = new Variable(i);
    RESULT.setPosition(ileft, iright);
:}         | IDENTIFIER:i COLON Type:t {:
    RESULT = new Variable(i, t);
    RESULT.setPosition(ileft, iright);
:}         ;

ParenExpression ::= LPAREN Expression:e RPAREN {:
    RESULT = new ParenExpression(e);
    RESULT.setPosition(eleft, eright);
:}                ;

IdentifierExpression ::= IDENTIFIER:i {:
    RESULT = new IdentifierExpression(i);
    RESULT.setPosition(ileft, iright);
:}                     ;

LiteralExpression ::= INTEGER_LITERAL:l {:
    RESULT = new LiteralExpression<Integer>(l);
    RESULT.setPosition(lleft, lright);
:}                  | STRING_LITERAL:l {:
    RESULT = new LiteralExpression<String>(l);
    RESULT.setPosition(lleft, lright);
:}                  | FLOAT_LITERAL:l {:
    RESULT = new LiteralExpression<Float>(l);
    RESULT.setPosition(lleft, lright);
:}                  | CHAR_LITERAL:l {:
    RESULT = new LiteralExpression<Character>(l);
    RESULT.setPosition(lleft, lright);
:}                  | BOOLEAN_LITERAL:l {:
    RESULT = new LiteralExpression<Boolean>(l);
    RESULT.setPosition(lleft, lright);
:}                  ;

Expression ::= DisjunctionExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | ConjunctionExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | EqualityExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | ComparisonExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | ContainingExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | RangeExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | AdditiveExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | MultiplicativeExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | PrefixExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | PostfixExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | ParenExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | IdentifierExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           | LiteralExpression:e {:
    RESULT = e;
    RESULT.setPosition(eleft, eright);
:}           ;

DisjunctionExpression ::= Expression:e1 OR Expression:e2 {:
    RESULT = new DisjunctionExpression(e1, e2);
    RESULT.setPosition(e1left, e1right);
:}                      ;

ConjunctionExpression ::= Expression:e1 AND Expression:e2 {:
    RESULT = new ConjunctionExpression(e1, e2);
    RESULT.setPosition(e1left, e1right);
:}                      ;

EqualityExpression ::= Expression:e1 EQ Expression:e2 {:
    RESULT = new EqualityExpression(e1, EqualityOperator.EQ, e2);
    RESULT.setPosition(e1left, e1right);
:}                   | Expression:e1 NEQ Expression:e2 {:
    RESULT = new EqualityExpression(e1, EqualityOperator.NEQ, e2);
    RESULT.setPosition(e1left, e1right);
:}                   | Expression:e1 ID_EQ Expression:e2 {:
    RESULT = new EqualityExpression(e1, EqualityOperator.ID_EQ, e2);
    RESULT.setPosition(e1left, e1right);
:}                   | Expression:e1 ID_NEQ Expression:e2 {:
    RESULT = new EqualityExpression(e1, EqualityOperator.ID_NEQ, e2);
    RESULT.setPosition(e1left, e1right);
:}                   ;

ComparisonExpression ::= Expression:e1 LT Expression:e2 {:
    RESULT = new ComparisonExpression(e1, ComparisonOperator.LT, e2);
    RESULT.setPosition(e1left, e1right);
:}                     | Expression:e1 GT Expression:e2 {:
    RESULT = new ComparisonExpression(e1, ComparisonOperator.GT, e2);
    RESULT.setPosition(e1left, e1right);
:}                     | Expression:e1 LTE Expression:e2 {:
    RESULT = new ComparisonExpression(e1, ComparisonOperator.LTE, e2);
    RESULT.setPosition(e1left, e1right);
:}                     | Expression:e1 GTE Expression:e2 {:
    RESULT = new ComparisonExpression(e1, ComparisonOperator.GTE, e2);
    RESULT.setPosition(e1left, e1right);
:}                     ;

ContainingExpression ::= Expression:e1 IN Expression:e2 {:
    RESULT = new ContainingExpression(e1, ContainingOperator.IN, e2);
    RESULT.setPosition(e1left, e1right);
:}                     | Expression:e1 NOT_IN Expression:e2 {:
    RESULT = new ContainingExpression(e1, ContainingOperator.NOT_IN, e2);
    RESULT.setPosition(e1left, e1right);
:}                     ;

RangeExpression ::= Expression:e1 RANGE Expression:e2 {:
    RESULT = new RangeExpression(e1, RangeOperator.RANGE, e2);
    RESULT.setPosition(e1left, e1right);
:}                | Expression:e1 DOWN_TO Expression:e2 {:
    RESULT = new RangeExpression(e1, RangeOperator.DOWN_TO, e2);
    RESULT.setPosition(e1left, e1right);
:}                | Expression:e1 UNTIL Expression:e2 {:
    RESULT = new RangeExpression(e1, RangeOperator.UNTIL, e2);
    RESULT.setPosition(e1left, e1right);
:}                | Expression:e1 RANGE Expression:e2 STEP Expression:e3 {:
    RESULT = new RangeExpression(e1, RangeOperator.RANGE, e2, e3);
    RESULT.setPosition(e1left, e1right);
:}                | Expression:e1 DOWN_TO Expression:e2 STEP Expression:e3 {:
    RESULT = new RangeExpression(e1, RangeOperator.DOWN_TO, e2, e3);
    RESULT.setPosition(e1left, e1right);
:}                | Expression:e1 UNTIL Expression:e2 STEP Expression:e3 {:
    RESULT = new RangeExpression(e1, RangeOperator.UNTIL, e2, e3);
    RESULT.setPosition(e1left, e1right);
:}                ;

AdditiveExpression ::= Expression:e1 ADD Expression:e2 {:
    RESULT = new AdditiveExpression(e1, AdditionOperator.ADD, e2);
    RESULT.setPosition(e1left, e1right);
:}                   | Expression:e1 SUB Expression:e2 {:
    RESULT = new AdditiveExpression(e1, AdditionOperator.SUB, e2);
    RESULT.setPosition(e1left, e1right);
:}                   ;

MultiplicativeExpression ::= Expression:e1 MUL Expression:e2 {:
    RESULT = new MultiplicativeExpression(e1, MultiplicationOperator.MUL, e2);
    RESULT.setPosition(e1left, e1right);
:}                         | Expression:e1 DIV Expression:e2 {:
    RESULT = new MultiplicativeExpression(e1, MultiplicationOperator.DIV, e2);
    RESULT.setPosition(e1left, e1right);
:}                         | Expression:e1 MOD Expression:e2 {:
    RESULT = new MultiplicativeExpression(e1, MultiplicationOperator.MOD, e2);
    RESULT.setPosition(e1left, e1right);
:}                         ;

PrefixExpression ::= INCR:i Expression:e {:
    RESULT = new PrefixExpression(PrefixOperator.INCR, e);
    RESULT.setPosition(ileft, iright);
:} %prec PRE_INCR  | DECR:d Expression:e {:
    RESULT = new PrefixExpression(PrefixOperator.DECR, e);
    RESULT.setPosition(dleft, dright);
:} %prec PRE_DECR  | SUB:s Expression:e {:
    RESULT = new PrefixExpression(PrefixOperator.UMINUS, e);
    RESULT.setPosition(sleft, sright);
:} %prec UMINUS    | ADD:a Expression:e {:
    RESULT = new PrefixExpression(PrefixOperator.UPLUS, e);
    RESULT.setPosition(aleft, aright);
:} %prec UPLUS     | NOT:n Expression:e {:
    RESULT = new PrefixExpression(PrefixOperator.NOT, e);
    RESULT.setPosition(nleft, nright);
:}                 ;

PostfixExpression ::= Expression:e Arguments:a {:
    RESULT = new PostfixExpression(e, new PostfixCallOperator(a));
    RESULT.setPosition(eleft, eright);
:}                  | Expression:e INCR {:
    RESULT = new PostfixExpression(e, PostfixMathOperator.INCR);
    RESULT.setPosition(eleft, eright);
:}                  | Expression:e DECR {:
    RESULT = new PostfixExpression(e, PostfixMathOperator.DECR);
    RESULT.setPosition(eleft, eright);
:}                  ;

Arguments ::= LPAREN:l RPAREN {:
    RESULT = new Arguments();
    RESULT.setPosition(lleft, lright);
:}          | LPAREN:l ArgumentList:al RPAREN {:
    RESULT = new Arguments(al);
    RESULT.setPosition(lleft, lright);
:}          ;

ArgumentList ::= Argument:a {:
    RESULT = Collections.singletonList(a);
:}             | ArgumentList:al COMMA Argument:a {:
    RESULT = new ArrayList(al);
    RESULT.add(a);
:}             ;

Argument ::= Expression:e {:
    RESULT = new Argument(e);
    RESULT.setPosition(eleft, eright);
:}         | IDENTIFIER:i ASSIGN Expression:e {:
    RESULT = new Argument(i, e);
    RESULT.setPosition(ileft, iright);
:}         ;

PropKind ::= VAR {:
    RESULT = PropKind.VAR;
:}         | VAL {:
    RESULT = PropKind.VAL;
:}         ;

Type ::= INT {:
    RESULT = Type.INT;
:}     | CHAR {:
    RESULT = Type.CHAR;
:}     | FLOAT {:
    RESULT = Type.FLOAT;
:}     | STRING {:
    RESULT = Type.STRING;
:}     | BOOLEAN {:
    RESULT = Type.BOOLEAN;
:}     | UNIT {:
    RESULT = Type.UNIT;
:}     ;
