package gr.hua.dit.it21685.compilers;

import java_cup.runtime.Symbol;
import org.slf4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static java.lang.Integer.parseInt;
import static java.lang.Float.parseFloat;
import static java.lang.Boolean.parseBoolean;
import static java.lang.String.format;
import static org.slf4j.LoggerFactory.getLogger;

%%

%class Lexer
%unicode
%public
%final
%integer
%line
%column
%cup

%{
    private static @NotNull Logger logger = getLogger(Lexer.class);

    private @Nullable Character ch = null;
    private @NotNull StringBuffer sb = new StringBuffer();

    private @NotNull Symbol token(@NotNull int type) {
        return token(type, null);
    }

    private @NotNull Symbol token(@NotNull int type, @Nullable Object value) {
        return new Symbol(type, yyline + 1, yycolumn + 1, value);
    }

    private @NotNull IllegalArgumentException reserved(@NotNull String msg) {
        return new IllegalArgumentException("Reserved " + msg);
    }
%}

Comment        = "//" .* \R?
               | "/*" [^*] ~"*/"
               | "/*" "*"+ "/"
Identifier     = [:jletter:] [:jletterdigit:]*
BooleanLiteral = "true" | "false"
IntegerLiteral = 0 | [1-9] [0-9]*
Exponent       = [Ee] [+-]? {IntegerLiteral}
FloatLiteral   = {IntegerLiteral} {Exponent}? [fF]?
               | {IntegerLiteral}? \. {IntegerLiteral}+ {Exponent}? [fF]?

%state STRING
%state CHAR

%%

<YYINITIAL> {
    /** Keywords */
    "break"                       { return token(ISymbols.BREAK); }
    "continue"                    { return token(ISymbols.CONTINUE); }
    "downTo"                      { return token(ISymbols.DOWN_TO); }
    "else"                        { return token(ISymbols.ELSE); }
    "for"                         { return token(ISymbols.FOR); }
    "fun"                         { return token(ISymbols.FUN); }
    "if"                          { return token(ISymbols.IF); }
    "in"                          { return token(ISymbols.IN); }
    "!in"                         { return token(ISymbols.NOT_IN); }
    "return"                      { return token(ISymbols.RETURN); }
    "step"                        { return token(ISymbols.STEP); }
    "until"                       { return token(ISymbols.UNTIL); }
    "val"                         { return token(ISymbols.VAL); }
    "var"                         { return token(ISymbols.VAR); }
    "while"                       { return token(ISymbols.WHILE); }
    "Boolean"                     { return token(ISymbols.BOOLEAN); }
    "Char"                        { return token(ISymbols.CHAR); }
    "Float"                       { return token(ISymbols.FLOAT); }
    "Int"                         { return token(ISymbols.INT); }
    "String"                      { return token(ISymbols.STRING); }
    "Unit"                        { return token(ISymbols.UNIT); }
    "this"                        { throw reserved("keyword"); }
    "null"                        { throw reserved("keyword"); }

    /** Literals */
    {IntegerLiteral}              { return token(ISymbols.INTEGER_LITERAL, parseInt(yytext())); }
    {FloatLiteral}                { return token(ISymbols.FLOAT_LITERAL, parseFloat(yytext())); }
    {BooleanLiteral}              { return token(ISymbols.BOOLEAN_LITERAL, parseBoolean(yytext())); }

    /** Identifiers */
    {Identifier}                  { return token(ISymbols.IDENTIFIER, yytext()); }

    /** Operators */
    "+"                           { return token(ISymbols.ADD); }
    "-"                           { return token(ISymbols.SUB); }
    "*"                           { return token(ISymbols.MUL); }
    "/"                           { return token(ISymbols.DIV); }
    "%"                           { return token(ISymbols.MOD); }
    "!"                           { return token(ISymbols.NOT); }
    "++"                          { return token(ISymbols.INCR); }
    "--"                          { return token(ISymbols.DECR); }
    ".."                          { return token(ISymbols.RANGE); }
    "&&"                          { return token(ISymbols.AND); }
    "||"                          { return token(ISymbols.OR); }
    "=="                          { return token(ISymbols.EQ); }
    "!="                          { return token(ISymbols.NEQ); }
    "<"                           { return token(ISymbols.LT); }
    ">"                           { return token(ISymbols.GT); }
    "<="                          { return token(ISymbols.LTE); }
    ">="                          { return token(ISymbols.GTE); }
    "==="                         { return token(ISymbols.ID_EQ); }
    "!=="                         { return token(ISymbols.ID_NEQ); }
    "="                           { return token(ISymbols.ASSIGN); }
    "+="                          { return token(ISymbols.ADD_ASSIGN); }
    "-="                          { return token(ISymbols.SUB_ASSIGN); }
    "*="                          { return token(ISymbols.MUL_ASSIGN); }
    "/="                          { return token(ISymbols.DIV_ASSIGN); }
    "%="                          { return token(ISymbols.MOD_ASSIGN); }
    "?"                           { throw reserved("operator"); }

    /** Separators */
    "{"                           { return token(ISymbols.LCURL); }
    "}"                           { return token(ISymbols.RCURL); }
    "("                           { return token(ISymbols.LPAREN); }
    ")"                           { return token(ISymbols.RPAREN); }
    ","                           { return token(ISymbols.COMMA); }
    ":"                           { return token(ISymbols.COLON); }
    ";"                           { return token(ISymbols.SEMI); }

    /** Comments */
    {Comment}                     { /* ignore */ }

    /** Whitespace */
    \s|\R                         { /* ignore */ }

    /** States */
    \'                            { yybegin(CHAR); }
    \"                            { sb.setLength(0); yybegin(STRING); }
}

<CHAR> {
    \'                            { yybegin(YYINITIAL); return token(ISymbols.CHAR_LITERAL, ch); }

    [^\r\n\'\\]                   { ch = yytext().charAt(0); }
    \\b                           { ch = '\b'; }
    \\t                           { ch = '\t'; }
    \\n                           { ch = '\n'; }
    \\r                           { ch = '\r'; }
    \\                            { ch = '\\'; }
    \\\'                          { ch = '\''; }
    \\\"                          { ch = '\"'; }
}

<STRING> {
    \"                            { yybegin(YYINITIAL); return token(ISymbols.STRING_LITERAL, sb.toString()); }

    [^\r\n\"\\]+                  { sb.append(yytext()); }
    \\b                           { sb.append('\b'); }
    \\t                           { sb.append('\t'); }
    \\n                           { sb.append('\n'); }
    \\r                           { sb.append('\r'); }
    \\                            { sb.append('\\'); }
    \\\'                          { sb.append('\''); }
    \\\"                          { sb.append('\"'); }
}

/* End of file */
<<EOF>>                           { return token(ISymbols.EOF); }

/** Error fallback */
.                                 {
          logger.error(format(
                  "(%d:%d) illegal character '%s'",
                  yyline + 1, yycolumn + 1, yytext()
          ));
          return token(ISymbols.error);
      }
