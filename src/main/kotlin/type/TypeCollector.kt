package gr.hua.dit.it21685.compilers.type

import gr.hua.dit.it21685.compilers.ast.*
import gr.hua.dit.it21685.compilers.symbol.Info

/** An AST visitor that collects type information. */
object TypeCollector : ASTVisitor<ASMType> {
    @Throws(ASTException::class)
    override fun invoke(node: ASTNode): ASMType = try {
        when (node) {
            is CompilationUnit -> {
                for (d in node) this(d)
                val main = node.table["main"]
                if (main == null || main.kind != null)
                    kotlin.error("The file does not have a main function.")
                node.table["main"] = main.copy(type = ASMType.getMethodType("([Ljava/lang/String;)V"))
                node.type = ASMType.VOID_TYPE
            }
            is PrefixExpression -> {
                node.type = this(node.expr)
            }
            is PostfixExpression -> {
                if (node.op is PostfixCallOperator) this(node.op.args)
                node.type = this(node.expr)
            }
            is DisjunctionExpression -> {
                if (!this(node.left).type.boolean)
                    error("LHS expression is not a boolean.")
                if (!this(node.right).type.boolean)
                    error("RHS expression is not a boolean.")
                node.type = ASMType.BOOLEAN_TYPE
            }
            is ConjunctionExpression -> {
                if (!this(node.left).type.boolean)
                    error("LHS expression is not a boolean.")
                if (!this(node.right).type.boolean)
                    error("RHS expression is not a boolean.")
                node.type = ASMType.BOOLEAN_TYPE
            }
            is EqualityExpression -> {
                val lhs = this(node.left).type
                val rhs = this(node.right).type
                if (lhs != rhs) error("Cannot equate ${lhs.className} to ${rhs.className}.")
                node.type = ASMType.BOOLEAN_TYPE
            }
            is ComparisonExpression -> {
                val lhs = this(node.left).type
                val rhs = this(node.right).type
                if (rhs.numeric && !lhs.numeric) error("LHS expression is not numeric.")
                if (lhs.numeric && !rhs.numeric) error("RHS expression is not numeric.")
                if (lhs != rhs) error("Cannot compare ${lhs.className} to ${rhs.className}.")
                node.type = ASMType.BOOLEAN_TYPE
            }
            is ContainingExpression -> {
                val lhs = this(node.left).type
                val rhs = this(node.right).type
                if (rhs != IntArray::class.type && rhs != String::class.type)
                    error("${rhs.className} cannot contain ${lhs.className}.")
                node.type = ASMType.BOOLEAN_TYPE
            }
            is RangeExpression -> {
                if (!this(node.left).type.integer)
                    error("LHS expression is not an int.")
                if (!this(node.right).type.integer)
                    error("RHS expression is not an int.")
                if (node.step != null && !this(node.step).type.integer)
                    error("Step expression is not an int.")
                if (node.op == RangeOperator.STEP)
                    node.error("Step operator cannot be used here.")
                node.type = IntArray::class.type
            }
            is AdditiveExpression -> {
                node.type = when (node.op) {
                    AdditionOperator.ADD -> this(node.left).type + this(node.right).type
                    AdditionOperator.SUB -> this(node.left).type - this(node.right).type
                }
            }
            is MultiplicativeExpression -> {
                node.type = when (node.op) {
                    MultiplicationOperator.MUL -> this(node.left).type * this(node.right).type
                    MultiplicationOperator.DIV -> this(node.left).type / this(node.right).type
                    MultiplicationOperator.MOD -> this(node.left).type % this(node.right).type
                }
            }
            is ParenExpression -> {
                node.type = this(node.expr)
            }
            is LiteralExpression<*> -> {
                node.type = when (node.value) {
                    is Int -> ASMType.INT_TYPE
                    is Float -> ASMType.FLOAT_TYPE
                    is Char -> ASMType.CHAR_TYPE
                    is Boolean -> ASMType.BOOLEAN_TYPE
                    is String -> String::class.type
                    else -> error("Invalid literal type.")
                }
            }
            is IdentifierExpression -> {
                node.type = node.table.lookup(node.toString())?.type
                        ?: node.error("Undefined reference: \"$node\".")
            }
            is DoWhileStatement -> {
                if (!this(node.expression).type.boolean)
                    error("Do-while loop expression must be boolean.")
                node.statement.function = node.function
                this(node.statement)
                node.type = ASMType.VOID_TYPE
            }
            is WhileStatement -> {
                if (!this(node.expression).type.boolean)
                    error("While loop expression must be boolean.")
                node.statement.function = node.function
                this(node.statement)
                node.type = ASMType.VOID_TYPE
            }
            is ForStatement -> {
                this(node.variable)
                this(node.expression).type.run {
                    if (sort != ASMType.ARRAY || elementType != ASMType.INT_TYPE)
                        error("For loop expression must return an array of int.")
                    node.table[node.variable.ident] = Info(node.variable.ident, elementType, PropKind.VAR)
                }
                node.statement.function = node.function
                this(node.statement)
                node.type = ASMType.VOID_TYPE
            }
            is IfStatement -> {
                if (!this(node.expression).type.boolean)
                    error("If statement expression must be boolean.")
                node.statement.function = node.function
                this(node.statement)
                node.type = ASMType.VOID_TYPE
            }
            is IfElseStatement -> {
                if (!this(node.expression).type.boolean)
                    error("If-else statement expression must be boolean.")
                node.statements.first.function = node.function
                this(node.statements.first)
                node.statements.second.function = node.function
                this(node.statements.second)
                node.type = ASMType.VOID_TYPE
            }
            is AssignStatement -> {
                node.type = ASMType.VOID_TYPE
                val ret = this(node.expression).type
                node.table.lookup(node.ident)?.run {
                    if (type != ret) error("Cannot assign ${ret.className} to ${type.className}.")
                    if (kind == PropKind.VAL) node.error("Cannot assign to a val property.")
                    if (kind == null) node.error("Cannot assign to a function.")
                } ?: node.error("Undefined reference: \"${node.ident}\".")
            }
            is ReturnStatement -> {
                if (node.function == null) node.error("Return detected outside a function.")
                if (node.expression != null) node.type = this(node.expression)
                if (node.function != node.type)
                    node.error("Function defined as ${node.function!!.className} but returns ${node.type.className}.")
            }
            is BreakStatement -> {
                node.type = ASMType.VOID_TYPE
            }
            is ContinueStatement -> {
                node.type = ASMType.VOID_TYPE
            }
            is ExpressionStatement -> {
                node.type = ASMType.VOID_TYPE
                this(node.expression)
            }
            is BlockStatement -> {
                for (s in node) this(s.apply { function = node.function })
                node.type = ASMType.VOID_TYPE
            }
            is FunctionDeclaration -> {
                if (node.table[node.ident] != null)
                    node.error("Function \"${node.ident}\" redefined.")
                ASMType.getMethodType(
                        node.type?.type ?: ASMType.VOID_TYPE,
                        *node.params.map { this(it) }.toTypedArray()
                ).let { type ->
                    (node as ASTNode).type = type
                    node.block.function = type.type
                    node.table[node.ident] = Info(node.ident, type, null)
                }
                this(node.block)
            }
            is PropertyDeclaration -> {
                if (node.table[node.variable.ident] != null)
                    node.error("Property \"${node.variable.ident}\" redefined.")
                node.type = node.variable.type?.type ?: node.expression?.let {
                    this(it).apply { (node.variable as ASTNode).type = type }
                } ?: error("Cannot infer property type.")
                node.table[node.variable.ident] = Info(node.variable.ident, node.type, node.kind)
            }
            is Arguments -> {
                for (a in node) this(a)
                node.type = ASMType.VOID_TYPE
            }
            is Argument -> {
                node.type = this(node.expression)
            }
            is Variable -> {
                if (node.type != null) (node as ASTNode).type = node.type.type
            }
            is FunctionParameters -> {
                for (p in node) this(p)
                node.type = ASMType.VOID_TYPE
            }
            is Parameter -> {
                (node as ASTNode).type = node.type.type
                node.table[node.ident] = Info(node.ident, node.type.type, PropKind.VAL)
            }
        }
        node.type
    } catch (exc: TypeException) {
        node.error(exc.message, exc)
    }
}
