package gr.hua.dit.it21685.compilers.type

/**
 * An enum class representing a type.
 *
 * @property type The Java byte code type.
 */
enum class Type(val type: ASMType) {
    /** The `Int` type. */ INT(ASMType.INT_TYPE),
    /** The `Char` type. */ CHAR(ASMType.CHAR_TYPE),
    /** The `String` type. */ STRING(String::class.type),
    /** The `Boolean` type. */ BOOLEAN(ASMType.BOOLEAN_TYPE),
    /** The `Float` type. */ FLOAT(ASMType.FLOAT_TYPE),
    /** The `Unit` type. */ UNIT(ASMType.VOID_TYPE);

    override fun toString() = name.toLowerCase().capitalize()
}
