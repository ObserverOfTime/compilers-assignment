@file:JvmName("TypeUtils")

package gr.hua.dit.it21685.compilers.type

/** A Java field or method type. */
internal typealias ASMType = org.objectweb.asm.Type

/**
 * Throws a type exception.
 *
 * @param message the exception message.
 * @throws [TypeException] an exception with the message.
 */
@Throws(TypeException::class)
internal fun error(message: String): Nothing = throw TypeException(message)

/**
 * Returns the type after adding two types.
 *
 * @receiver the LHS type.
 * @param that the RHS type.
 * @throws [TypeException] if the addition cannot be applied.
 */
@JvmName("add")
@Throws(TypeException::class)
operator fun ASMType.plus(that: ASMType) = when (this) {
    ASMType.INT_TYPE -> if (that.numeric) that else error("Cannot add ${that.className} to int.")
    ASMType.FLOAT_TYPE -> if (that.numeric) this else error("Cannot add ${that.className} to float.")
    ASMType.CHAR_TYPE -> if (that == ASMType.INT_TYPE) this else error("Cannot add ${that.className} to char.")
    String::class.type -> this
    else -> error("Cannot add ${that.className} to $className.")
}

/**
 * Returns the type after subtracting two types.
 *
 * @receiver the LHS type.
 * @param that the RHS type.
 * @throws [TypeException] if the subtraction cannot be applied.
 */
@JvmName("subtract")
@Throws(TypeException::class)
operator fun ASMType.minus(that: ASMType) = when (this) {
    ASMType.INT_TYPE -> if (that.numeric) that else error("Cannot subtract ${that.className} from int.")
    ASMType.FLOAT_TYPE -> if (that.numeric) this else error("Cannot subtract ${that.className} from float.")
    ASMType.CHAR_TYPE -> if (that == ASMType.INT_TYPE) this else error("Cannot subtract ${that.className} from char.")
    else -> error("Cannot subtract ${that.className} from $className.")
}

/**
 * Returns the type after multiplying two types.
 *
 * @receiver the LHS type.
 * @param that the RHS type.
 * @throws [TypeException] if the multiplication cannot be applied.
 */
@JvmName("multiply")
@Throws(TypeException::class)
operator fun ASMType.times(that: ASMType) = when (this) {
    ASMType.INT_TYPE -> if (that.numeric) that else error("Cannot multiply int with ${that.className}.")
    ASMType.FLOAT_TYPE -> if (that.numeric) this else error("Cannot multiply float with ${that.className}.")
    else -> error("Cannot multiply $className with ${that.className}.")
}

/**
 * Returns the type after dividing two types.
 *
 * @receiver the LHS type.
 * @param that the RHS type.
 * @throws [TypeException] if the division cannot be applied.
 */
@JvmName("divide")
@Throws(TypeException::class)
operator fun ASMType.div(that: ASMType) = when (this) {
    ASMType.INT_TYPE -> if (that.numeric) that else error("Cannot divide int with ${that.className}.")
    ASMType.FLOAT_TYPE -> if (that.numeric) this else error("Cannot divide float with ${that.className}.")
    else -> error("Cannot divide $className with ${that.className}.")
}

/**
 * Returns the type after applying modulo to two types.
 *
 * @receiver the LHS type.
 * @param that the RHS type.
 * @throws [TypeException] if the modulo cannot be applied.
 */
@JvmName("modulo")
@Throws(TypeException::class)
operator fun ASMType.rem(that: ASMType) = that.takeIf {
    numeric && it == ASMType.INT_TYPE
} ?: error("Cannot apply modulo of ${that.className} to $className.")

/** Used to check if a type is numeric. */
@get:JvmName("isNumeric")
val ASMType.numeric get() = sort == ASMType.INT || sort == ASMType.FLOAT

/** Used to check if a type is a string. */
@get:JvmName("isString")
val ASMType.string get() = descriptor == "Ljava/lang/String;"

/** Used to check if a type is an integer. */
@get:JvmName("isInteger")
val ASMType.integer get() = sort == ASMType.INT

/** Used to check if a type is a float. */
@get:JvmName("isFloat")
val ASMType.float get() = sort == ASMType.FLOAT

/** Used to check if a type is boolean. */
@get:JvmName("isBoolean")
val ASMType.boolean get() = sort == ASMType.BOOLEAN

/** Used to check if a type represents a method. */
@get:JvmName("isMethod")
val ASMType.method get() = sort == ASMType.METHOD

/** Used to get the return type of a method or field. */
val ASMType.type: ASMType get() = if (method) returnType else this

/** The Java type of the class. */
internal inline val kotlin.reflect.KClass<*>.type
    @JvmSynthetic get() = ASMType.getType(java)!!
