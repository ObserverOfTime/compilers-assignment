package gr.hua.dit.it21685.compilers.type

/**
 * The base class for all type errors and exceptions.
 *
 * @property message The detail message string.
 * @property cause The cause of this exception.
 */
class TypeException @JvmOverloads constructor(
        override val message: String? = null,
        override val cause: Throwable? = null
) : Throwable(message)
