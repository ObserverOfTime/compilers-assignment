package gr.hua.dit.it21685.compilers.ast

/** An AST visitor that simply prints the node. */
object ASTPrinter : ASTVisitor<Unit> {
    @Throws(ASTException::class)
    override operator fun invoke(node: ASTNode) = println(node)
}
