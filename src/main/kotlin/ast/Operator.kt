package gr.hua.dit.it21685.compilers.ast

/** Base operator interface. */
interface Operator {
    override fun toString(): String
}

/** Unary operator interface. */
interface UnaryOperator : Operator

/** Binary operator interface */
interface BinaryOperator : Operator

/**
 * Equality operators.
 *
 * @param repr the string representation of the operator.
 */
enum class EqualityOperator(private val repr: String) : BinaryOperator {
    /** The `==` operator. */ EQ("=="),
    /** The `!=` operator. */ NEQ("!="),
    /** The `===` operator. */ ID_EQ("==="),
    /** The `!==` operator. */ ID_NEQ("!==");

    override fun toString() = repr
}

/**
 * Comparison operators.
 *
 * @param repr the string representation of the operator.
 */
enum class ComparisonOperator(private val repr: String) : BinaryOperator {
    /** The `<` operator. */ LT("<"),
    /** The `>` operator. */ GT(">"),
    /** The `<=` operator. */ LTE("<="),
    /** The `>=` operator. */ GTE(">=");

    override fun toString() = repr
}

/**
 * Boolean operators.
 *
 * @param repr the string representation of the operator.
 */
enum class BooleanOperator(private val repr: String) : BinaryOperator {
    /** The `||` operator. */ OR("||"),
    /** The `&&` operator. */ AND("&&");

    override fun toString() = repr
}

/**
 * Addition operators.
 *
 * @param repr the string representation of the operator.
 */
enum class AdditionOperator(private val repr: String) : BinaryOperator {
    /** The `+` operator. */ ADD("+"),
    /** The `-` operator. */ SUB("-");

    override fun toString() = repr
}

/**
 * Multiplication operators.
 *
 * @param repr the string representation of the operator.
 */
enum class MultiplicationOperator(private val repr: String) : BinaryOperator {
    /** The `*` operator. */ MUL("*"),
    /** The `/` operator. */ DIV("/"),
    /** The `%` operator. */ MOD("%");

    override fun toString() = repr
}

/**
 * UnaryOperator prefix operators.
 *
 * @param repr the string representation of the operator.
 */
enum class PrefixOperator(private val repr: String) : UnaryOperator {
    /** The unary `+` operator. */ UPLUS("+"),
    /** The unary `-` operator. */ UMINUS("-"),
    /** The unary `++` operator. */ INCR("++"),
    /** The unary `--` operator. */ DECR("--"),
    /** The unary `!` operator. */ NOT("!");

    override fun toString() = repr
}

/** Container interface for postfix operations. */
interface PostfixOperator : UnaryOperator

/**
 * Postfix function call.
 *
 * @property args The arguments of the call.
 */
class PostfixCallOperator(val args: Arguments) : PostfixOperator {
    override fun toString() = args.toString()
}

/**
 * Unary postfix operators.
 *
 * @param repr the string representation of the operator.
 */
enum class PostfixMathOperator(private val repr: String) : PostfixOperator {
    /** The unary `++` operator. */ INCR("++"),
    /** The unary `--` operator. */ DECR("--");

    override fun toString() = repr
}

/**
 * Containing operators.
 *
 * @param repr the string representation of the operator.
 */
enum class ContainingOperator(private val repr: String) : BinaryOperator {
    /** The `in` operator. */ IN("in"),
    /** The `!in` operator. */ NOT_IN("!in");

    override fun toString() = repr
}

/**
 * Range operators.
 *
 * @param repr the string representation of the operator.
 */
enum class RangeOperator(private val repr: String) : BinaryOperator {
    /** The `..` operator. */ RANGE(".."),
    /** The `downTo` operator. */ DOWN_TO("downTo"),
    /** The `until` operator. */ UNTIL("until"),
    /** The `step` operator. */ STEP("step");

    override fun toString() = repr
}

/**
 * Assignment operators.
 *
 * @param repr the string representation of the operator.
 */
enum class AssignmentOperator(private val repr: String) : BinaryOperator {
    /** The `=` operator. */ ASSIGN("="),
    /** The `+=` operator. */ ADD_ASSIGN("+="),
    /** The `-=` operator. */ SUB_ASSIGN("-="),
    /** The `*=` operator. */ MUL_ASSIGN("*="),
    /** The `/=` operator. */ DIV_ASSIGN("/="),
    /** The `%=` operator. */ MOD_ASSIGN("%=");

    override fun toString() = repr
}
