package gr.hua.dit.it21685.compilers.ast

/**
 * An enum representing the a property kind.
 */
enum class PropKind {
    /** A read-only property. */ VAL,
    /** A read-write property. */ VAR;

    override fun toString() = name.toLowerCase()
}
