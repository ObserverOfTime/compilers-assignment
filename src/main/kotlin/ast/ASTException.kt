package gr.hua.dit.it21685.compilers.ast

/**
 * The base class for all AST errors and exceptions.
 *
 * @property message The detail message string.
 * @property cause The cause of this exception.
 */
class ASTException @JvmOverloads constructor(
        override val message: String? = null,
        override val cause: Throwable? = null
) : Throwable(message)
