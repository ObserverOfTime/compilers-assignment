package gr.hua.dit.it21685.compilers.ast

/**
 * AST visitor interface.
 *
 * @param R the return type of the invocation.
 */
interface ASTVisitor<out R> {
    /**
     * Calls the visitor on an AST node.
     *
     * @param node an AST node object.
     * @throws [ASTException] if something went wrong.
     */
    @Throws(ASTException::class)
    operator fun invoke(node: ASTNode): R
}
