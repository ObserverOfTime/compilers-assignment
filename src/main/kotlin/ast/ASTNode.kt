package gr.hua.dit.it21685.compilers.ast

import gr.hua.dit.it21685.compilers.type.Type

/**
 * Abstract syntax tree node.
 *
 * Every node class must inherit from this one.
 */
sealed class ASTNode {
    /** The properties of the node. */
    private val properties: MutableMap<String, Any> = mutableMapOf()

    /** The line of the node in the file. */
    var line: Int = 0

    /** The column of the node in the file. */
    var column: Int = 0

    /**
     * Sets the position of the node in the file.
     *
     * @param line the number of the line.
     * @param column the number of the column.
     */
    fun setPosition(line: Int, column: Int) {
        this.line = line
        this.column = column
    }

    /**
     * Returns a node property by name.
     *
     * @param name the name of the property.
     * @return the value of the property.
     */
    @JvmName("getProperty")
    operator fun get(name: String) = properties[name]

    /**
     * Sets a node property by name.
     *
     * @param name the name of the property.
     * @param value the value of the property.
     */
    @JvmName("setProperty")
    operator fun set(name: String, value: Any) {
        properties[name] = value
    }

    /**
     * Checks whether the node contains a property.
     *
     * @param name the name of the property.
     * @return ``true`` if the property is in the node.
     */
    @JvmName("hasProperty")
    operator fun contains(name: String) = name in properties

    abstract override fun toString(): String
}

/** Base expression node class. */
sealed class Expression : ASTNode()

/**
 * A unary expression node.
 *
 * @param T The type of the operator.
 */
sealed class UnaryExpression<out T : UnaryOperator> : Expression() {
    /** The parent expression node. */
    abstract val expr: Expression

    /** The operator of the expression. */
    abstract val op: T
}

/**
 * A binary expression node.
 *
 * @param T The type of the operator.
 */
sealed class BinaryExpression<out T : BinaryOperator> : Expression() {
    /** The LHS expression node. */
    abstract val left: Expression

    /** The RHS expression node. */
    abstract val right: Expression

    /** The operator of the expression. */
    abstract val op: T
}

/** Base statement node class. */
sealed class Statement : ASTNode()

/** Base declaration node class. */
sealed class Declaration : Statement()

/**
 * A node representing the full compilation unit.
 *
 * @property declarations A list of [Declaration] nodes.
 */
class CompilationUnit(private val declarations: List<Declaration>) : ASTNode(), Iterable<Declaration> by declarations {
    /** Construct an empty compilation unit. */
    constructor() : this(emptyList())

    override fun toString() = declarations.joinToString("\n")
}

/**
 * A node representing a function declaration.
 *
 * @property ident The name of the function.
 * @property params The parameters of the function.
 * @property type The return type of the function (optional).
 * @property block The body of the function.
 */
class FunctionDeclaration(
        val ident: String,
        val params: FunctionParameters,
        val type: Type?,
        val block: BlockStatement
) : Declaration() {
    /** Construct a function declaration with inferred type. */
    constructor(ident: String, params: FunctionParameters, block: BlockStatement) : this(ident, params, null, block)

    override fun toString() = "fun $ident$params${type?.let { ": $it" } ?: ""} $block"
}

/**
 * A node representing a property declaration.
 *
 * @property kind The kind of the property.
 * @property variable The property variable.
 * @property expression The value of the property (optional).
 */
class PropertyDeclaration(val kind: PropKind, val variable: Variable, val expression: Expression?) : Declaration() {
    /** Construct a property declaration without a value. */
    constructor(kind: PropKind, variable: Variable) : this(kind, variable, null)

    override fun toString() = "$kind $variable" + (expression?.let { " = $it;" } ?: ";")
}

/**
 * A node representing a `do...while` loop.
 *
 * @property statement The body of the loop.
 * @property expression The condition of the loop.
 */
class DoWhileStatement(val statement: Statement, val expression: Expression) : Statement() {
    override fun toString() = "do $statement while ($expression);"
}

/**
 * A node representing a `while` loop.
 *
 * @property expression The condition of the loop.
 * @property statement The body of the loop.
 */
class WhileStatement(val expression: Expression, val statement: Statement) : Statement() {
    override fun toString() = "while ($expression) $statement"
}

/**
 * A node representing a `for` loop.
 *
 * @property variable The loop variable.
 * @property expression An iterable expression.
 * @property statement The body of the loop.
 */
class ForStatement(val variable: Variable, val expression: RangeExpression, val statement: Statement) : Statement() {
    override fun toString() = "for ($variable in $expression) $statement"
}

/**
 * A node representing an `if` statement.
 *
 * @property expression The condition of the statement.
 * @property statement The body of the statement.
 */
class IfStatement(val expression: Expression, val statement: Statement) : Statement() {
    override fun toString() = "if ($expression) $statement"
}

/**
 * A node representing an `if...else` statement.
 *
 * @property expression The condition of the statement.
 * @property statements The two bodies of the statement.
 */
class IfElseStatement(val expression: Expression, val statements: Pair<Statement, Statement>) : Statement() {
    /** Construct an `if...else` statement from two statements. */
    constructor(expression: Expression, first: Statement, second: Statement) : this(expression, first to second)

    override fun toString() = "if ($expression) ${statements.first} else ${statements.second}"
}

/**
 * A node representing an assignment.
 *
 * @property ident The assigned identifier.
 * @property op An assignment operator.
 * @property expression The assigned expression.
 */
class AssignStatement(val ident: String, val op: AssignmentOperator, val expression: Expression) : Statement() {
    override fun toString() = "$ident $op $expression;"
}

/**
 * A node representing a `return` statement.
 *
 * @property expression The returned expression (optional).
 */
class ReturnStatement(val expression: Expression?) : Statement() {
    /** Construct a `return` statement with no expression. */
    constructor() : this(null)

    override fun toString() = expression?.let { "return $it;" } ?: "return;"
}

/** A node representing a `break` statement. */
class BreakStatement : Statement() {
    override fun toString() = "break;"
}

/** A node representing a `continue` statement. */
class ContinueStatement : Statement() {
    override fun toString() = "continue;"
}

/**
 * A node representing a simple expression statement.
 *
 * @property expression The expression of the statement.
 */
class ExpressionStatement(val expression: Expression) : Statement() {
    override fun toString() = "$expression;"
}

/**
 * A node representing a block of statements.
 *
 * @property statements The statements of the block.
 */
class BlockStatement(private val statements: List<Statement>) : Statement(), Iterable<Statement> by statements {
    /** Construct an empty block statement. */
    constructor() : this(emptyList())

    override fun toString() = statements.joinToString("\n", "{\n", "\n}")
}

/** A node representing a comparison. */
class ComparisonExpression(
        override val left: Expression,
        override val op: ComparisonOperator,
        override val right: Expression
) : BinaryExpression<ComparisonOperator>() {
    override fun toString() = "$left $op $right"
}

/** A node representing a disjunction. */
class DisjunctionExpression(
        override val left: Expression,
        override val right: Expression
) : BinaryExpression<BooleanOperator>() {
    override val op = BooleanOperator.OR

    override fun toString() = "$left $op $right"
}

/** A node representing a conjunction. */
class ConjunctionExpression(
        override val left: Expression,
        override val right: Expression
) : BinaryExpression<BooleanOperator>() {
    override val op = BooleanOperator.AND

    override fun toString() = "$left $op $right"
}

/** A node representing an equality check. */
class EqualityExpression(
        override val left: Expression,
        override val op: EqualityOperator,
        override val right: Expression
) : BinaryExpression<EqualityOperator>() {
    override fun toString() = "$left $op $right"
}

/** A node representing a containment check. */
class ContainingExpression(
        override val left: Expression,
        override val op: ContainingOperator,
        override val right: Expression
) : BinaryExpression<ContainingOperator>() {
    override fun toString() = "$left $op $right"
}

/**
 * A node representing a range operation.
 *
 * @property step The step of the range (optional).
 */
class RangeExpression(
        override val left: Expression,
        override val op: RangeOperator,
        override val right: Expression,
        val step: Expression?
) : BinaryExpression<RangeOperator>() {
    /** Construct a range operation without a step. */
    constructor(left: Expression, op: RangeOperator, right: Expression) : this(left, op, right, null)

    override fun toString() = buildString {
        append(left)
        append(if (op == RangeOperator.RANGE) op else " $op ")
        append(right)
        if (step != null) append(" step $step")
    }
}

/** A node representing an additive operation. */
class AdditiveExpression(
        override val left: Expression,
        override val op: AdditionOperator,
        override val right: Expression
) : BinaryExpression<AdditionOperator>() {
    override fun toString() = "$left $op $right"
}

/** A node representing a multiplicative operation. */
class MultiplicativeExpression(
        override val left: Expression,
        override val op: MultiplicationOperator,
        override val right: Expression
) : BinaryExpression<MultiplicationOperator>() {
    override fun toString() = "$left $op $right"
}

/** A node representing a prefix operation. */
class PrefixExpression(
        override val op: PrefixOperator,
        override val expr: Expression
) : UnaryExpression<PrefixOperator>() {
    override fun toString() = "$op$expr"
}

/** A node representing a postfix operation. */
class PostfixExpression(
        override val expr: Expression,
        override val op: PostfixOperator
) : UnaryExpression<PostfixOperator>() {
    override fun toString() = "$expr$op"
}

/**
 * A node representing a parenthesised expression.
 *
 * @property expr The inner expression.
 */
class ParenExpression(val expr: Expression) : Expression() {
    override fun toString() = "($expr)"
}

/**
 * A node representing a literal value.
 *
 * @param T The type of the literal.
 * @property value The value of the literal.
 */
class LiteralExpression<T : Any>(val value: T) : Expression() {
    /** Escapes special characters in a string. */
    private fun escape(str: String) = str
            .replace("\\", "\\\\")
            .replace("\"", "\\\"")
            .replace("\'", "\\\'")
            .replace("\r", "\\r")
            .replace("\n", "\\n")
            .replace("\t", "\\t")
            .replace("\b", "\\b")

    override fun toString() = when (value) {
        is Char -> "'${escape("$value")}'"
        is String -> "\"${escape(value)}\""
        is Int, is Float, is Boolean -> value.toString()
        else -> kotlin.error("Invalid literal type: ${value::class.java.name}")
    }
}

/**
 * A node representing an identifier.
 *
 * @param ident the name of the identifier.
 */
class IdentifierExpression(private val ident: String) : Expression(), CharSequence by ident {
    override fun toString() = ident
}

/**
 * A node representing a function call's arguments.
 *
 * @property arguments The arguments of the call.
 */
class Arguments(private val arguments: List<Argument>) : ASTNode(), Iterable<Argument> by arguments {
    /** Construct an empty function call. */
    constructor() : this(emptyList())

    /** The number of the arguments. */
    val size = arguments.size

    override fun toString() = arguments.joinToString(", ", "(", ")")
}

/**
 * A node representing an argument.
 *
 * @property ident The name of the argument (optional).
 * @property expression The value of the argument.
 */
class Argument(val ident: String?, val expression: Expression) : ASTNode() {
    /** Construct an unnamed argument. */
    constructor(expression: Expression) : this(null, expression)

    override fun toString() = (ident?.let { "$it = " } ?: "") + expression
}

/**
 * A node representing a variable.
 *
 * @property ident The name of the variable.
 * @property type The type of the variable (optional).
 */
class Variable(val ident: String, val type: Type?) : ASTNode() {
    /** Construct a variable with an inferred type. */
    constructor(ident: String) : this(ident, null)

    override fun toString() = ident + (type?.let { ": $it" } ?: "")
}

/**
 * A node representing a function parameter declaration.
 *
 * @property parameters The parameters of the function.
 */
class FunctionParameters(private val parameters: List<Parameter>) : ASTNode(), Iterable<Parameter> by parameters {
    /** Construct an empty function parameter declaration. */
    constructor() : this(emptyList())

    override fun toString() = parameters.joinToString(", ", "(", ")")
}

/**
 * A node representing a function parameter.
 *
 * @property ident The name of the parameter.
 * @property type The type of the parameter.
 */
class Parameter(val ident: String, val type: Type) : ASTNode() {
    override fun toString() = "$ident: $type"
}
