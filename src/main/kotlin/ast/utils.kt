@file:JvmName("ASTUtils")

package gr.hua.dit.it21685.compilers.ast

import gr.hua.dit.it21685.compilers.bytecode.LocalIndexPool
import gr.hua.dit.it21685.compilers.symbol.Info
import gr.hua.dit.it21685.compilers.symbol.SymbolTable
import org.objectweb.asm.Type as ASMType

/** The name of the symbol table property. */
private const val TABLE = "SYMBOL_TABLE"

/** The name of the type property. */
private const val TYPE = "TYPE"

/** The name of the boolean expression property. */
private const val BOOLEAN_EXPR = "BOOLEAN_EXPR"

/** The name of the function type property. */
private const val FUN_TYPE = "FUNCTION_TYPE"

/** The name of the local index pool property. */
private const val POOL = "LOCAL_INDEX_POOL"

/**
 * Returns a node property cast to [T].
 *
 * @receiver the node holding the property.
 * @param T the type of the property.
 * @param name the name of the property.
 */
@JvmSynthetic inline fun <reified T : Any> ASTNode.prop(name: String) = this[name] as? T

/**
 * The symbol table property of the node.
 *
 * @receiver the node holding the symbol table.
 * @throws [ASTException] if the table is missing.
 */
@get:JvmName("getSymbolTable")
@set:JvmName("setSymbolTable")
@get:Throws(ASTException::class)
var ASTNode.table: SymbolTable<Info>
    get() = prop(TABLE) ?: error("Symbol table not found.")
    set(value) { this[TABLE] = value }

/**
 * The type property of the node.
 *
 * @receiver the node holding the symbol table.
 */
var ASTNode.type: ASMType
    get() = prop(TYPE) ?: ASMType.VOID_TYPE
    set(value) { this[TYPE] = value }

/**
 * Defines whether the node is a boolean expression.
 *
 * @receiver the node holding the expression.
 */
@get:JvmName("isBooleanExpression")
@set:JvmName("setBooleanExpression")
var ASTNode.boolean: Boolean
    get() = prop(BOOLEAN_EXPR) ?: false
    set(value) { this[BOOLEAN_EXPR] = value }

/**
 * The type of the function containing the node.
 *
 * @receiver the node holding the expression.
 */
@get:JvmName("getFunctionType")
@set:JvmName("setFunctionType")
var ASTNode.function: ASMType?
    get() = prop(FUN_TYPE)
    set(value) { if (value != null) this[FUN_TYPE] = value }

/**
 * The local index pool property of the node.
 *
 * @receiver the node holding the local index pool.
 * @throws [ASTException] if the pool is missing.
 */
@get:JvmName("getLocalIndexPool")
@set:JvmName("setLocalIndexPool")
@get:Throws(ASTException::class)
var ASTNode.pool: LocalIndexPool
    get() = prop(POOL) ?: error("Local index pool not found.")
    set(value) { this[POOL] = value }

/**
 * Throws an error on the given node.
 *
 * @receiver the node of the error.
 * @param message an error message.
 * @param cause the cause of the error.
 * @throws [ASTException] an exception with the message.
 */
@Throws(ASTException::class)
fun ASTNode.error(message: String?, cause: Throwable? = null): Nothing =
        throw ASTException("($line:$column) $message", cause)
