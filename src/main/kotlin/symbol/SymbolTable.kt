package gr.hua.dit.it21685.compilers.symbol

/**
 * Symbol table interface.
 *
 * @param E the type of the symbols.
 */
interface SymbolTable<E> {
    /** The top symbols. */
    val top: Collection<E>

    /** All the symbols. */
    val all: Collection<E>

    /** Clears the symbols. */
    fun clear()

    /**
     * Looks up a symbol on all levels.
     *
     * @param name the name of the symbol.
     * @return the symbol, if found.
     */
    fun lookup(name: String): E?

    /**
     * Looks up a symbol at the top level.
     *
     * @param name the name of the symbol.
     * @return the symbol, if found.
     */
    operator fun get(name: String): E?

    /**
     * Sets the value of a symbol.
     *
     * @param name the name of the symbol.
     * @param element the value of the symbol.
     */
    operator fun set(name: String, element: E)
}
