package gr.hua.dit.it21685.compilers.symbol

/**
 * A symbol table using storing the symbols in a [HashMap].
 *
 * @param E the type of the symbols.
 * @property table A map of symbols.
 * @property parent The parent symbol table.
 */
class HashSymbolTable<E>(
        private val table: MutableMap<String, E> = mutableMapOf(),
        private val parent: SymbolTable<E>? = null
) : SymbolTable<E> {
    override val top get() = table.values

    override val all get() = parent?.let { it.all + top } ?: top

    override fun clear() = table.clear()

    override fun lookup(name: String): E? = this[name] ?: parent?.lookup(name)

    override operator fun get(name: String) = table[name]

    override operator fun set(name: String, element: E) {
        table[name] = element
    }

    override fun toString() = (parent?.let { "$it -> " } ?: "") + table
}
