package gr.hua.dit.it21685.compilers.symbol

import gr.hua.dit.it21685.compilers.ast.PropKind
import gr.hua.dit.it21685.compilers.type.ASMType

/**
 * Data class that contains symbol information.
 *
 * @property id The id of the symbol.
 * @property type The type of the symbol.
 * @property kind The kind of the property, `null` if it's a function.
 * @property index The symbol's position the local pool.
 */
data class Info(val id: String, val type: ASMType, val kind: PropKind?, var index: Int = -1) {
    override fun equals(other: Any?) = when {
        this === other -> true
        other !is Info -> false
        else -> id == other.id
    }

    override fun hashCode() = id.hashCode() * 31

    companion object {
        /** Built-in [print] method info. */
        @JvmField val PRINT_METHOD = Info("print", ASMType.getMethodType("(Ljava/lang/Object;)V"), null)
    }
}
