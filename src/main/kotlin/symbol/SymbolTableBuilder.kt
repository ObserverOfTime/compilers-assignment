package gr.hua.dit.it21685.compilers.symbol

import gr.hua.dit.it21685.compilers.ast.*

/** An AST visitor that builds a symbol table. */
class SymbolTableBuilder : ASTVisitor<SymbolStack> {
    /** A stack of symbol tables. */
    private val stack = SymbolStack()

    @Throws(ASTException::class)
    override fun invoke(node: ASTNode): SymbolStack {
        when (node) {
            is CompilationUnit -> stack.scope {
                node.table = it
                node.table["print"] = Info.PRINT_METHOD
                for (d in node) this(d)
            }
            is PrefixExpression -> {
                node.table = stack.element()
                this(node.expr)
            }
            is PostfixExpression -> {
                node.table = stack.element()
                if (node.op is PostfixCallOperator) this(node.op.args)
                this(node.expr)
            }
            is DisjunctionExpression -> {
                node.table = stack.element()
                this(node.left)
                this(node.right)
            }
            is ConjunctionExpression -> {
                node.table = stack.element()
                this(node.left)
                this(node.right)
            }
            is EqualityExpression -> {
                node.table = stack.element()
                this(node.left)
                this(node.right)
            }
            is ComparisonExpression -> {
                node.table = stack.element()
                this(node.left)
                this(node.right)
            }
            is ContainingExpression -> {
                node.table = stack.element()
                this(node.left)
                this(node.right)
            }
            is RangeExpression -> {
                node.table = stack.element()
                this(node.left)
                this(node.right)
                if (node.step != null) this(node.step)
            }
            is AdditiveExpression -> {
                node.table = stack.element()
                this(node.left)
                this(node.right)
            }
            is MultiplicativeExpression -> {
                node.table = stack.element()
                this(node.left)
                this(node.right)
            }
            is ParenExpression -> {
                node.table = stack.element()
                this(node.expr)
            }
            is LiteralExpression<*> -> {
                node.table = stack.element()
            }
            is IdentifierExpression -> {
                node.table = stack.element()
            }
            is DoWhileStatement -> {
                node.table = stack.element()
                this(node.statement)
                this(node.expression)
            }
            is WhileStatement -> {
                node.table = stack.element()
                this(node.expression)
                this(node.statement)
            }
            is ForStatement -> stack.scope {
                node.table = it
                this(node.expression)
                this(node.statement)
            }
            is IfStatement -> {
                node.table = stack.element()
                this(node.expression)
                this(node.statement)
            }
            is IfElseStatement -> {
                node.table = stack.element()
                this(node.expression)
                this(node.statements.first)
                this(node.statements.second)
            }
            is AssignStatement -> {
                node.table = stack.element()
                this(node.expression)
            }
            is ReturnStatement -> {
                node.table = stack.element()
                if (node.expression != null) this(node.expression)
            }
            is BreakStatement -> {
                node.table = stack.element()
            }
            is ContinueStatement -> {
                node.table = stack.element()
            }
            is ExpressionStatement -> {
                node.table = stack.element()
                this(node.expression)
            }
            is BlockStatement -> stack.scope {
                node.table = it
                for (s in node) this(s)
            }
            is FunctionDeclaration -> {
                node.table = stack.element()
                this(node.params)
                this(node.block)
            }
            is PropertyDeclaration -> {
                node.table = stack.element()
                this(node.variable)
                if (node.expression != null) this(node.expression)
            }
            is Arguments -> {
                node.table = stack.element()
                for (a in node) this(a)
            }
            is Argument -> {
                node.table = stack.element()
                this(node.expression)
            }
            is Variable -> {
                node.table = stack.element()
            }
            is FunctionParameters -> {
                node.table = stack.element()
                for (p in node) this(p)
            }
            is Parameter -> {
                node.table = stack.element()
            }
        }
        return stack
    }

    companion object {
        /**
         * Creates and invokes a new instance.
         *
         * @param node an AST node object.
         * @return the symbol stack of the instance.
         */
        @JvmSynthetic
        operator fun invoke(node: ASTNode) = SymbolTableBuilder()(node)
    }
}
