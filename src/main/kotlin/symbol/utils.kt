@file:JvmName("SymbolUtils")

package gr.hua.dit.it21685.compilers.symbol

/** A stack of symbol tables. */
typealias SymbolStack = java.util.ArrayDeque<SymbolTable<Info>>

/**
 * Calls the given block on a new stack scope.
 *
 * @receiver a symbol stack object.
 * @param block a symbol table function block.
 */
@JvmSynthetic
internal inline fun SymbolStack.scope(block: (SymbolTable<Info>) -> Unit) {
    with(HashSymbolTable(parent = peek())) {
        push(this)
        block(this)
        pop()
    }
}
