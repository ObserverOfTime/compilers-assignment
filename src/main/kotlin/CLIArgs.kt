package gr.hua.dit.it21685.compilers

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.SystemExitException
import com.xenomachina.argparser.default
import java.io.File
import java.io.FileNotFoundException
import java.nio.file.Path

/**
 * A class representing the CLI arguments.
 *
 * @param parser the parser of the arguments.
 */
class CLIArgs(parser: ArgParser) {
    /** A list of input files. */
    val files by parser.positionalList("input file(s)") {
        try {
            File(this)
        } catch (ex: FileNotFoundException) {
            throw SystemExitException("No such file ($this)", 1)
        }
    }

    /** The encoding of the files. */
    val encoding by parser.storing("-e", "--encoding", help = "file encoding (default: UTF-8)") {
        try {
            charset(this)
        } catch (ex: IllegalArgumentException) {
            throw SystemExitException("Invalid encoding ($this)", 1)
        }
    }.default(Charsets.UTF_8)

    /** The output result. */
    val result by parser.storing("-r", "--result", help = "result: ${Result.options} (default: ${Result.default})") {
        try {
            Result.valueOf(toUpperCase())
        } catch (ex: IllegalArgumentException) {
            throw SystemExitException("Invalid result ($this)", 1)
        }
    }.default(Result.default)

    /** The compilation output folder. */
    val output by parser.storing("-o", "--output", help = "compilation output folder") {
        val file = File(this)
        when {
            file.isDirectory -> file.toPath()
            !file.exists() -> file.also { it.mkdirs() }.toPath()
            else -> throw SystemExitException("File exists ($this)", 1)
        }
    }.default<Path?>(null)

    /** The verbosity level of the program. */
    val verbose by parser.counting("-v", "--verbose", help = "enable verbose logging").default(0)

    /** The version of the program. */
    private val version by parser.option<Unit>("-V", "--version", help = "show version and exit") {
        throw SystemExitException("v${VERSION ?: "???"}", 0)
    }.default(Unit)

    /** An enum representing the type of the result. */
    enum class Result {
        BYTECODE, IR, TEXT;

        internal companion object {
            /** The result type options. */
            @JvmField internal val options = values().joinToString(", ", "{", "}") { it.name.toLowerCase() }

            /** The default result type. */
            @JvmField internal val default = values().first()
        }
    }

    companion object {
        /** The name of the program. */
        @JvmField val PROGRAM: String? = this::class.java.`package`.implementationTitle

        /** The version of the program. */
        @JvmField val VERSION: String? = this::class.java.`package`.implementationVersion
    }
}
