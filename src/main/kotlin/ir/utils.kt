@file:JvmName("IRUtils")

package gr.hua.dit.it21685.compilers.ir

import gr.hua.dit.it21685.compilers.ast.ASTNode
import gr.hua.dit.it21685.compilers.ast.prop

/** The name of the true list property. */
private const val TRUE_LIST = "IR_TRUE_LIST"

/** The name of the false list property. */
private const val FALSE_LIST = "IR_FALSE_LIST"

/** The name of the next list property. */
private const val NEXT_LIST = "IR_NEXT_LIST"

/** The name of the break list property. */
private const val BREAK_LIST = "IR_BREAK_LIST"

/** The name of the continue list property. */
private const val CONT_LIST = "IR_CONTINUE_LIST"

/**
 * The true list property of the node.
 *
 * @receiver the node holding the true list.
 */
var ASTNode.trueList: MutableList<GotoInstr>
    get() = prop(TRUE_LIST) ?: mutableListOf<GotoInstr>().also { trueList = it }
    set(value) { this[TRUE_LIST] = value }

/**
 * The false list property of the node.
 *
 * @receiver the node holding the false list.
 */
var ASTNode.falseList: MutableList<GotoInstr>
    get() = prop(FALSE_LIST) ?: mutableListOf<GotoInstr>().also { falseList = it }
    set(value) { this[FALSE_LIST] = value }

/**
 * The next list property of the node.
 *
 * @receiver the node holding the next list.
 */
var ASTNode.nextList: MutableList<GotoInstr>
    get() = prop(NEXT_LIST) ?: mutableListOf<GotoInstr>().also { nextList = it }
    set(value) { this[NEXT_LIST] = value }

/**
 * The break list property of the node.
 *
 * @receiver the node holding the break list.
 */
var ASTNode.breakList: MutableList<GotoInstr>
    get() = prop(BREAK_LIST) ?: mutableListOf<GotoInstr>().also { breakList = it }
    set(value) { this[BREAK_LIST] = value }

/**
 * The continue list property of the node.
 *
 * @receiver the node holding the continue list.
 */
var ASTNode.continueList: MutableList<GotoInstr>
    get() = prop(CONT_LIST) ?: mutableListOf<GotoInstr>().also { continueList = it }
    set(value) { this[CONT_LIST] = value }
