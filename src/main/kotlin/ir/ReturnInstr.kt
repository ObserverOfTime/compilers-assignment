package gr.hua.dit.it21685.compilers.ir

/** A `return` instruction. */
class ReturnInstr(override val arg1: String?) : Instruction() {
    override fun toString() = "return" + if (arg1 != null) " $arg1" else ""
}
