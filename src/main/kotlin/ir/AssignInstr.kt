package gr.hua.dit.it21685.compilers.ir

import gr.hua.dit.it21685.compilers.ast.AssignmentOperator

/** An assignment instruction. */
class AssignInstr(
        val op: AssignmentOperator,
        override val arg1: String,
        override val result: String
) : Instruction() {
    /**
     * Construct an assignment instruction using [AssignmentOperator.ASSIGN].
     *
     * @param arg1 the first argument.
     * @param result the result.
     */
    constructor(arg1: String, result: String) : this(AssignmentOperator.ASSIGN, arg1, result)

    override fun toString() = "$result $op $arg1"
}
