package gr.hua.dit.it21685.compilers.ir

/** Abstract three-address instruction class. */
abstract class Instruction {
    /** The first argument. */
    open val arg1: String?
        get() = error("${this::class.simpleName} has no arg1.")

    /** The second argument. */
    open val arg2: String?
        get() = error("${this::class.simpleName} has no arg2.")

    /** The result. */
    open val result: String?
        get() = error("${this::class.simpleName} has no result.")

    abstract override fun toString(): String
}
