package gr.hua.dit.it21685.compilers.ir

import gr.hua.dit.it21685.compilers.ast.PrefixOperator

/** A prefix operation instruction. */
class PrefixInstr(
        val op: PrefixOperator,
        override val arg2: String,
        override val result: String
) : Instruction() {
    override fun toString() = "$result = $op$arg2"
}
