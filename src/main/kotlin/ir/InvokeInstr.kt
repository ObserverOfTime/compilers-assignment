package gr.hua.dit.it21685.compilers.ir

/**
 * A function invocation instruction.
 *
 * @param arg2 the number of params.
 */
class InvokeInstr(
        override val arg1: String,
        arg2: Int,
        override val result: String?
) : Instruction() {
    override val arg2 = arg2.toString()

    override fun toString() = (if (result != null) "$result = " else "") + "invoke $arg1, $arg2"
}
