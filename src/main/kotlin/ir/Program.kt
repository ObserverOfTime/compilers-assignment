package gr.hua.dit.it21685.compilers.ir

/**
 * An intermediate representation program.
 *
 * @property instrs The instructions of the program.
 */
class Program private constructor(
        private val instrs: MutableList<Instruction>
) : MutableList<Instruction> by instrs {
    /** Construct a new blank program. */
    constructor() : this(mutableListOf())

    /** The number of labels of the program. */
    private var labels = 0L

    /** Add a new label to the program. */
    @get:JvmName("addLabel")
    val label get() = LabelInstr("\$L${++labels}").also { add(it) }

    override fun toString() = instrs.joinToString("\n").replace("\n-- ", "\n\n-- ")
}
