package gr.hua.dit.it21685.compilers.ir

import gr.hua.dit.it21685.compilers.ast.Operator

/**
 * A conditional jump instruction
 *
 * @property cond The condition to jump.
 */
class CondJumpInstr(
        val op: Operator?,
        override val arg1: String,
        override val arg2: String?,
        val cond: Boolean = true,
        override var target: LabelInstr? = null
) : GotoInstr(target) {
    init {
        require((op == null) xor (arg2 != null)) {
            "Either both or neither of op, arg2 must be null."
        }
    }

    override fun toString() = buildString {
        append(if (cond) "if" else "unless")
        append(" $arg1 ")
        if (op != null) append("$op $arg2 ")
        append("goto ${target?.result ?: "_"}")
    }
}

