package gr.hua.dit.it21685.compilers.ir

/** A label instruction. */
class LabelInstr(override val result: String) : Instruction() {
    override fun toString() = "$result:"
}
