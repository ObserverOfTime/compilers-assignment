package gr.hua.dit.it21685.compilers.ir

/** A function parameter instruction. */
class ParamInstr(override val arg1: String) : Instruction() {
    override fun toString() = "param $arg1"
}
