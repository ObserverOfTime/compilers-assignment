package gr.hua.dit.it21685.compilers.ir

import gr.hua.dit.it21685.compilers.ast.BinaryOperator

/** A binary operation instruction. */
class BinaryInstr(
        val op: BinaryOperator,
        override val arg1: String,
        override val arg2: String,
        override val result: String
) : Instruction() {
    override fun toString() = "$result = $arg1 $op $arg2"
}
