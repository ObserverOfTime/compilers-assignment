package gr.hua.dit.it21685.compilers.ir

import gr.hua.dit.it21685.compilers.ast.*
import org.objectweb.asm.Type
import java.util.ArrayDeque

/** An AST visitor that creates an intermediate representation of the code. */
class IRGenerator : ASTVisitor<Program> {
    /** The number of temporary variables of the program. */
    private var temps = 0L

    /** Create a new temporary variable for the program. */
    @get:JvmName("createTemp")
    private val temp get() = "\$t${temps++}"

    /** The program of the visitor. */
    private val program = Program()

    /** The stack of the program. */
    private val stack = ArrayDeque<String>()

    @Suppress("DuplicatedCode")
    @Throws(ASTException::class)
    override fun invoke(node: ASTNode): Program = program.also {
        when (node) {
            is CompilationUnit -> node.iterator().let {
                var decl: Declaration? = null
                while (it.hasNext()) {
                    if (decl != null && decl.nextList.isNotEmpty())
                        backpatch(decl.nextList, program.label)
                    decl = it.next()
                    this(decl)
                    if (decl.breakList.isNotEmpty())
                        decl.error("Break detected outside a loop.")
                    if (decl.continueList.isNotEmpty())
                        decl.error("Continue detected outside a loop.")
                }
                if (decl != null && decl.nextList.isNotEmpty())
                    backpatch(decl.nextList, program.label)
            }
            is PrefixExpression -> {
                this(node.expr)
                if (node.op == PrefixOperator.NOT && node.boolean) {
                    val cond = CondJumpInstr(null, stack.pop(), null, false)
                    val goto = GotoInstr()
                    program += cond
                    program += goto
                } else {
                    val instr = PrefixInstr(node.op, stack.pop(), temp)
                    stack.push(instr.result)
                    program += instr
                }
            }
            is PostfixExpression -> {
                this(node.expr)
                val instr: Instruction
                if (node.op is PostfixCallOperator) {
                    val func = stack.pop()
                    this(node.op.args)
                    if (node.table.lookup(func)!!.type.returnType == Type.VOID_TYPE) {
                        instr = InvokeInstr(func, node.op.args.size, null)
                    } else {
                        instr = InvokeInstr(func, node.op.args.size, temp)
                        stack.push(instr.result!!)
                    }
                } else {
                    instr = PostfixInstr(node.op, stack.pop(), temp)
                    stack.push(instr.result)
                }
                program += instr
            }
            is ComparisonExpression -> {
                this(node.left)
                val t1 = stack.pop()
                this(node.right)
                val t2 = stack.pop()
                if (node.boolean) {
                    val cond = CondJumpInstr(node.op, t1, t2)
                    val goto = GotoInstr()
                    program += cond
                    program += goto
                    node.trueList.add(cond)
                    node.falseList.add(goto)
                } else {
                    val instr = BinaryInstr(node.op, t1, t2, temp)
                    stack.push(instr.result)
                    program += instr
                }
            }
            is DisjunctionExpression -> {
                if (node.boolean) {
                    this(node.left)
                    val lhs = CondJumpInstr(node.op, stack.pop(), temp)
                    val goto1 = GotoInstr()
                    this(node.right)
                    val rhs = CondJumpInstr(node.op, stack.pop(), temp)
                    val goto2 = GotoInstr()
                    program += arrayOf(lhs, goto1, rhs, goto2)
                    node.trueList.add(lhs)
                    node.falseList.add(goto1)
                    node.trueList.add(rhs)
                    node.falseList.add(goto2)
                } else {
                    this(node.left)
                    val t1 = stack.pop()
                    this(node.right)
                    val t2 = stack.pop()
                    val instr = BinaryInstr(node.op, t1, t2, temp)
                    stack.push(instr.result)
                    program += instr
                }
            }
            is ConjunctionExpression -> {
                if (node.boolean) {
                    this(node.left)
                    val lhs = CondJumpInstr(node.op, stack.pop(), temp)
                    val goto1 = GotoInstr()
                    this(node.right)
                    val rhs = CondJumpInstr(node.op, stack.pop(), temp)
                    val goto2 = GotoInstr()
                    program += arrayOf(lhs, goto1, rhs, goto2)
                    node.trueList.add(lhs)
                    node.falseList.add(goto1)
                    node.trueList.add(rhs)
                    node.falseList.add(goto2)
                } else {
                    this(node.left)
                    val t1 = stack.pop()
                    this(node.right)
                    val t2 = stack.pop()
                    val instr = BinaryInstr(node.op, t1, t2, temp)
                    stack.push(instr.result)
                    program += instr
                }
            }
            is EqualityExpression -> {
                this(node.left)
                val t1 = stack.pop()
                this(node.right)
                val t2 = stack.pop()
                if (node.boolean) {
                    val cond = CondJumpInstr(node.op, t1, t2)
                    val goto = GotoInstr()
                    program += cond
                    program += goto
                    node.trueList.add(cond)
                    node.falseList.add(goto)
                } else {
                    val instr = BinaryInstr(node.op, t1, t2, temp)
                    stack.push(instr.result)
                    program += instr
                }
            }
            is ContainingExpression -> {
                this(node.left)
                val t1 = stack.pop()
                this(node.right)
                val t2 = stack.pop()
                if (node.boolean) {
                    val cond = CondJumpInstr(node.op, t1, t2)
                    val goto = GotoInstr()
                    program += cond
                    program += goto
                    node.trueList.add(cond)
                    node.falseList.add(goto)
                } else {
                    val instr = BinaryInstr(node.op, t1, t2, temp)
                    stack.push(instr.result)
                    program += instr
                }
            }
            is RangeExpression -> {
                this(node.left)
                val t1 = stack.pop()
                this(node.right)
                val t2 = stack.pop()
                var instr = BinaryInstr(node.op, t1, t2, temp)
                stack.push(instr.result)
                program += instr
                if (node.step != null) {
                    this(node.step)
                    instr = BinaryInstr(RangeOperator.STEP, instr.result, stack.pop(), temp)
                    stack.push(instr.result)
                    program += instr
                }
            }
            is AdditiveExpression -> {
                this(node.left)
                val t1 = stack.pop()
                this(node.right)
                val t2 = stack.pop()
                val instr = BinaryInstr(node.op, t1, t2, temp)
                stack.push(instr.result)
                program += instr
            }
            is MultiplicativeExpression -> {
                this(node.left)
                val t1 = stack.pop()
                this(node.right)
                val t2 = stack.pop()
                val instr = BinaryInstr(node.op, t1, t2, temp)
                stack.push(instr.result)
                program += instr
            }
            is ParenExpression -> {
                this(node.expr)
                val instr = AssignInstr(stack.pop(), temp)
                stack.push(instr.result)
                program += instr
            }
            is LiteralExpression<*> -> {
                if (node.boolean) {
                    val cond = CondJumpInstr(null, node.toString(), null)
                    val goto = GotoInstr()
                    program += cond
                    program += goto
                    if (node.value as Boolean) {
                        node.trueList.add(cond)
                        node.falseList.add(goto)
                    } else {
                        node.falseList.add(cond)
                        node.trueList.add(goto)
                    }
                } else {
                    val instr = AssignInstr(node.toString(), temp)
                    stack.push(instr.result)
                    program += instr
                }
            }
            is IdentifierExpression -> {
                stack.push(node.toString())
            }
            is FunctionDeclaration -> {
                program += FunctionInstr(node.ident)
                this(node.block)
                if (node.block.nextList.isNotEmpty())
                    backpatch(node.block.nextList, program.label)
                // this(node.params)
            }
            is PropertyDeclaration -> {
                if (node.expression != null) {
                    this(node.expression)
                    --temps
                    program += AssignInstr(program.removeAt(program.lastIndex).arg1!!, node.variable.ident)
                }
                // this(node.variable)
            }
            is DoWhileStatement -> {
                node.expression.boolean = true
                val begin = program.label
                this(node.statement)
                node.nextList.addAll(node.statement.breakList)
                val expr = program.label
                backpatch(node.statement.nextList, expr)
                backpatch(node.statement.continueList, expr)
                this(node.expression)
                node.nextList.addAll(node.expression.falseList)
                backpatch(node.expression.trueList, begin)
            }
            is WhileStatement -> {
                node.expression.boolean = true
                val begin = program.label
                this(node.expression)
                val stmt = program.label
                backpatch(node.expression.trueList, stmt)
                this(node.statement)
                program += GotoInstr(begin)
                backpatch(node.statement.nextList, begin)
                backpatch(node.statement.continueList, begin)
                node.nextList.addAll(node.expression.falseList)
                node.nextList.addAll(node.statement.breakList)
            }
            is ForStatement -> {
                this(node.expression)
                val begin = program.label
                val cond = CondJumpInstr(ContainingOperator.IN, node.variable.ident, stack.pop(), false)
                node.expression.falseList.add(cond)
                program += cond
                this(node.statement)
                val stmt = program.label
                backpatch(node.expression.trueList, stmt)
                program += AssignInstr(AssignmentOperator.valueOf(
                        if (node.expression.op == RangeOperator.DOWN_TO) "SUB_ASSIGN" else "ADD_ASSIGN"
                ), node.expression.step?.let {
                    this(node.expression.step)
                    stack.pop()
                } ?: "1", node.variable.ident)
                program += GotoInstr(begin)
                backpatch(node.statement.continueList, stmt)
                node.nextList.addAll(node.expression.falseList)
                node.nextList.addAll(node.statement.breakList)
            }
            is IfStatement -> {
                node.expression.boolean = true
                this(node.expression)
                val label = program.label
                this(node.statement)
                backpatch(node.expression.trueList, label)
                node.breakList.addAll(node.statement.breakList)
                node.continueList.addAll(node.statement.continueList)
                node.nextList.addAll(node.expression.falseList)
                node.nextList.addAll(node.statement.nextList)
            }
            is IfElseStatement -> {
                node.expression.boolean = true
                this(node.expression)
                val label1 = program.label
                this(node.statements.first)
                val goto = GotoInstr()
                program += goto
                val label2 = program.label
                this(node.statements.second)
                backpatch(node.expression.trueList, label1)
                backpatch(node.expression.falseList, label2)
                node.breakList.addAll(node.statements.first.breakList)
                node.breakList.addAll(node.statements.second.breakList)
                node.continueList.addAll(node.statements.first.continueList)
                node.continueList.addAll(node.statements.second.continueList)
                node.nextList.addAll(node.statements.first.nextList)
                node.nextList.addAll(node.statements.second.nextList)
                node.nextList.add(goto)
            }
            is AssignStatement -> {
                this(node.expression)
                program += AssignInstr(node.op, stack.pop(), node.ident)
            }
            is ReturnStatement -> {
                program += ReturnInstr(node.expression?.let {
                    this(it)
                    stack.pop()
                })
            }
            is BreakStatement -> {
                val goto = GotoInstr()
                program += goto
                node.breakList.add(goto)
            }
            is ContinueStatement -> {
                val goto = GotoInstr()
                program += goto
                node.continueList.add(goto)
            }
            is ExpressionStatement -> {
                this(node.expression)
            }
            is BlockStatement -> node.iterator().let {
                val breaks = mutableListOf<GotoInstr>()
                val contns = mutableListOf<GotoInstr>()
                var stat: Statement? = null
                while (it.hasNext()) {
                    if (stat != null && stat.nextList.isNotEmpty())
                        backpatch(stat.nextList, program.label)
                    stat = it.next()
                    this(stat)
                    breaks += stat.breakList
                    contns += stat.continueList
                }
                stat?.run { node.nextList = nextList }
                node.breakList = breaks
                node.continueList = contns
            }
            is Arguments -> {
                for (a in node) this(a)
            }
            is Argument -> {
                this(node.expression)
                program += ParamInstr(stack.pop())
            }
            is Variable -> {
                // NOOP
            }
            is FunctionParameters -> {
                // for (p in node) this(p)
            }
            is Parameter -> {
                // NOOP
            }
        }
    }

    companion object {
        /**
         * Backpatch a list of instructions with the given target.
         *
         * @param list a list of `goto` instructions.
         * @param target the target label to go to.
         */
        @JvmStatic fun backpatch(list: List<GotoInstr>?, target: LabelInstr) = list?.forEach { it.target = target }

        /**
         * Creates and invokes a new instance.
         *
         * @param node an AST node object.
         * @return the program of the instance.
         */
        @JvmSynthetic operator fun invoke(node: ASTNode) = IRGenerator()(node)
    }
}
