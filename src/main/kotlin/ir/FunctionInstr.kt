package gr.hua.dit.it21685.compilers.ir

/** A function declaration instruction. */
class FunctionInstr(override val result: String) : Instruction() {
    override fun toString() = "-- $result --"
}
