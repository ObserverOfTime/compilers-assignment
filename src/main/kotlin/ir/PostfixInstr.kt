package gr.hua.dit.it21685.compilers.ir

import gr.hua.dit.it21685.compilers.ast.PostfixOperator

/** A postfix operation instruction. */
class PostfixInstr(
        val op: PostfixOperator,
        override val arg1: String,
        override val result: String
) : Instruction() {
    override fun toString() = "$result = $arg1$op"
}
