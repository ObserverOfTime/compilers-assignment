package gr.hua.dit.it21685.compilers.ir

/**
 * A `goto` instruction.
 *
 * @property target The target to go to.
 */
open class GotoInstr(open var target: LabelInstr? = null) : Instruction() {
    override val arg1 get() = target?.result

    override fun toString() = "goto ${arg1 ?: "_"}"
}
