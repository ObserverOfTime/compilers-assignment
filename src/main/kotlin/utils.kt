@file:JvmName("Utils")

package gr.hua.dit.it21685.compilers

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.pattern.color.ANSIConstants
import ch.qos.logback.core.pattern.color.ForegroundCompositeConverterBase

/**
 * The log level of the logger.
 *
 * @receiver a `KLogger` object.
 */
internal var mu.KLogger.level: Level
    get() = (underlyingLogger as Logger).level
    set(value) { (underlyingLogger as Logger).level = value }

/** Custom log coloriser. */
class LogColoriser : ForegroundCompositeConverterBase<ILoggingEvent>() {
    override fun getForegroundColorCode(event: ILoggingEvent) = when (event.level) {
        Level.ERROR -> ANSIConstants.BOLD + ANSIConstants.RED_FG
        Level.WARN -> ANSIConstants.BOLD + ANSIConstants.YELLOW_FG
        Level.INFO -> ANSIConstants.BLUE_FG
        Level.DEBUG -> ANSIConstants.CYAN_FG
        Level.TRACE -> ANSIConstants.MAGENTA_FG
        else -> ANSIConstants.DEFAULT_FG
    }
}
