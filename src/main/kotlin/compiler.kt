@file:JvmName("Compiler")

package gr.hua.dit.it21685.compilers

import ch.qos.logback.classic.Level
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import gr.hua.dit.it21685.compilers.ast.ASTNode
import gr.hua.dit.it21685.compilers.ast.ASTPrinter
import gr.hua.dit.it21685.compilers.ast.CompilationUnit
import gr.hua.dit.it21685.compilers.bytecode.BytecodeGenerator
import gr.hua.dit.it21685.compilers.bytecode.LocalIndexBuilder
import gr.hua.dit.it21685.compilers.bytecode.ReloadingClassLoader
import gr.hua.dit.it21685.compilers.ir.IRGenerator
import gr.hua.dit.it21685.compilers.symbol.SymbolTableBuilder
import gr.hua.dit.it21685.compilers.type.TypeCollector

private val logger = mu.KotlinLogging.logger {}

private inline operator fun <R> Parser.invoke(block: ASTNode.() -> R) =
        use { it.node<CompilationUnit>().run(block) }

fun main(args: Array<String>) = mainBody(CLIArgs.PROGRAM) {
    ArgParser(args).parseInto(::CLIArgs).run {
        if (verbose > 0) logger.level = Level.INFO
        for (file in files) {
            logger.info { "Scanning file: $file" }
            try {
                val parse = Parser(file.reader(encoding))
                when (result) {
                    CLIArgs.Result.BYTECODE -> parse {
                        SymbolTableBuilder(this)
                        TypeCollector(this)
                        LocalIndexBuilder(this)
                        val cls = file.nameWithoutExtension
                        val bytes = if (verbose > 1) BytecodeGenerator(cls, this)
                        else BytecodeGenerator(cls)(this).toByteArray()
                        if (output != null) {
                            logger.info { "Writing $cls class to $output" }
                            output!!.resolve("$cls.class").toFile().writeBytes(bytes)
                        } else {
                            try { // wait for log
                                logger.info { "Executing $file" }
                                Thread.sleep(20L)
                            } catch (ex: InterruptedException) {}
                            try {
                                val loader = ReloadingClassLoader().also { it[cls] = bytes }
                                loader[cls].getMethod("main", Array<String>::class.java)(null, null)
                            } catch (err: VerifyError) {
                                logger.error(err) { err.message }
                            }
                        }
                    }
                    CLIArgs.Result.IR -> parse {
                        SymbolTableBuilder(this)
                        TypeCollector(this)
                        println(IRGenerator(this))
                    }
                    CLIArgs.Result.TEXT -> parse {
                        if (verbose > 1) Parser.logger.level = Level.DEBUG
                        ASTPrinter(this)
                        logger.info { "Successfully parsed file" }
                    }
                }
            } catch (ex: Throwable) {
                logger.error(ex) { ex.message }
            }
        }
    }
}
