@file:JvmName("BytecodeUtils")

package gr.hua.dit.it21685.compilers.bytecode

import gr.hua.dit.it21685.compilers.ast.ASTNode
import gr.hua.dit.it21685.compilers.ast.prop
import org.objectweb.asm.tree.JumpInsnNode

/** The name of the true list property. */
private const val TRUE_LIST = "ASM_TRUE_LIST"

/** The name of the false list property. */
private const val FALSE_LIST = "ASM_FALSE_LIST"

/** The name of the next list property. */
private const val NEXT_LIST = "ASM_NEXT_LIST"

/** The name of the break list property. */
private const val BREAK_LIST = "ASM_BREAK_LIST"

/** The name of the continue list property. */
private const val CONT_LIST = "ASM_CONTINUE_LIST"

/**
 * The true list property of the node.
 *
 * @receiver the node holding the true list.
 */
var ASTNode.trueList: MutableList<JumpInsnNode>
    get() = prop(TRUE_LIST) ?: mutableListOf<JumpInsnNode>().also { trueList = it }
    set(value) { this[TRUE_LIST] = value }

/**
 * The false list property of the node.
 *
 * @receiver the node holding the false list.
 */
var ASTNode.falseList: MutableList<JumpInsnNode>
    get() = prop(FALSE_LIST) ?: mutableListOf<JumpInsnNode>().also { falseList = it }
    set(value) { this[FALSE_LIST] = value }

/**
 * The next list property of the node.
 *
 * @receiver the node holding the next list.
 */
var ASTNode.nextList: MutableList<JumpInsnNode>
    get() = prop(NEXT_LIST) ?: mutableListOf<JumpInsnNode>().also { nextList = it }
    set(value) { this[NEXT_LIST] = value }

/**
 * The break list property of the node.
 *
 * @receiver the node holding the break list.
 */
var ASTNode.breakList: MutableList<JumpInsnNode>
    get() = prop(BREAK_LIST) ?: mutableListOf<JumpInsnNode>().also { breakList = it }
    set(value) { this[BREAK_LIST] = value }

/**
 * The continue list property of the node.
 *
 * @receiver the node holding the continue list.
 */
var ASTNode.continueList: MutableList<JumpInsnNode>
    get() = prop(CONT_LIST) ?: mutableListOf<JumpInsnNode>().also { continueList = it }
    set(value) { this[CONT_LIST] = value }

/** A stack of local index pools. */
typealias LocalIndexStack = java.util.ArrayDeque<LocalIndexPool>

/**
 * Calls the given block on a new stack scope.
 *
 * @receiver a local index pool stack object.
 * @param block a local index pool function block.
 */
@JvmSynthetic internal inline fun LocalIndexStack.scope(block: (LocalIndexPool) -> Unit) {
    with(LocalIndexPool()) {
        push(this)
        block(this)
        pop()
    }
}
