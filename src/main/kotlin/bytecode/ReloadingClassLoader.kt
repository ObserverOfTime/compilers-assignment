package gr.hua.dit.it21685.compilers.bytecode

/**
 * A class loader that supports dynamically adding classes.
 *
 * @param parent the parent class loader.
 */
class ReloadingClassLoader(parent: ClassLoader = getSystemClassLoader()) : ClassLoader(parent) {
    /** A map of class names to class objects. */
    private val map = mutableMapOf<String, Class<*>>()

    /**
     * Registers the given class.
     *
     * @param name the name of the class.
     * @param bytes the bytecode of the class.
     */
    @JvmName("register")
    operator fun set(name: String, bytes: ByteArray) {
        var key = name
        if (key.startsWith('/')) key = name.substring(1)
        if (key.endsWith(".class")) key = key.substring(0, key.length - 6);
        map[key.replace('/', '.')] = defineClass(name, bytes, 0, bytes.size)
    }

    /** An alias of [loadClass]. */
    @Throws(ClassNotFoundException::class)
    @JvmSynthetic operator fun get(name: String): Class<*> = loadClass(name)

    @Throws(ClassNotFoundException::class)
    override fun loadClass(name: String): Class<*> = map[name] ?: super.loadClass(name)
}

