package gr.hua.dit.it21685.compilers.bytecode

/**
 * A pool of local variable indices.
 *
 * @property max The maximum number of indices
 */
class LocalIndexPool @JvmOverloads constructor(private val max: Int = Int.MAX_VALUE) {
    /** The currently used indices. */
    private val used: java.util.SortedSet<Int> = java.util.TreeSet()

    /** The current number of indices. */
    var locals: Int = 0
        private set

    /**
     * Adds a new index to the pool.
     *
     * @return the newly acquired index.
     * @throws [RuntimeException] if the pool is full.
     */
    @Throws(RuntimeException::class)
    fun acquireIndex(): Int {
        for (i in 0 until max) {
            if (i !in used) {
                used += i
                if (i > locals) locals = i
                return i
            }
        }
        throw RuntimeException("The pool cannot contain more indices.")
    }

    /**
     * Removes the index from the pool.
     *
     * @param index the index to be released.
     */
    fun releaseIndex(index: Int) { used -= index }
}


