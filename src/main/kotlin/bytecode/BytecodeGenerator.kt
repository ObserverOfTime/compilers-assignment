package gr.hua.dit.it21685.compilers.bytecode

import gr.hua.dit.it21685.compilers.ast.*
import gr.hua.dit.it21685.compilers.type.float
import gr.hua.dit.it21685.compilers.type.string
import gr.hua.dit.it21685.compilers.type.type
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes.*
import org.objectweb.asm.tree.*
import org.objectweb.asm.util.TraceClassVisitor


/**
 * An AST visitor that generates the program's bytecode.
 *
 * @property name The name of the class.
 */
class BytecodeGenerator(private val name: String) : ASTVisitor<ClassWriter> {
    /** The class writer of the program. */
    private val writer = ClassWriter(ClassWriter.COMPUTE_MAXS or ClassWriter.COMPUTE_FRAMES)

    /** The class node of the program. */
    private val klass = ClassNode().also {
        it.access = ACC_PUBLIC or ACC_SUPER
        it.version = V1_5
        it.name = name
        it.superName = "java/lang/Object"
    }

    /** The current method node of the program. */
    private var method = MethodNode(ACC_PUBLIC, "<init>", "()V", null, null).apply {
        instructions.add(VarInsnNode(ALOAD, 0))
        instructions.add(MethodInsnNode(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false))
        instructions.add(InsnNode(RETURN))
        visitMaxs(0, 0)
        klass.methods.add(this)
    }

    /** Creates and adds a new label to the program. */
    @get:JvmName("addLabel")
    private val label
        get() = LabelNode().also { method.instructions.add(it) }

    @Throws(ASTException::class)
    override fun invoke(node: ASTNode): ClassWriter {
        when (node) {
            is CompilationUnit -> {
                method = MethodNode(ACC_STATIC, "<clinit>", "()V", null, null)
                node.iterator().let {
                    var decl: Declaration? = null
                    while (it.hasNext()) {
                        if (decl != null && decl.nextList.isNotEmpty())
                            backpatch(decl.nextList, this.label)
                        decl = it.next()
                        this(decl)
                        if (decl.breakList.isNotEmpty())
                            decl.error("Break detected outside a loop.")
                        if (decl.continueList.isNotEmpty())
                            decl.error("Continue detected outside a loop.")
                    }
                    if (decl != null && decl.nextList.isNotEmpty())
                        backpatch(decl.nextList, this.label)
                }
                method.instructions.add(InsnNode(RETURN))
                method.visitMaxs(0, 0)
                klass.methods.add(1, method)
                klass.accept(writer)
            }
            is PrefixExpression -> {
                when (node.op) {
                    PrefixOperator.UPLUS -> {
                        this(node.expr)
                    }
                    PrefixOperator.UMINUS -> {
                        this(node.expr)
                        method.instructions.add(InsnNode(INEG))
                    }
                    PrefixOperator.INCR -> {
                        val index = node.table.lookup(node.expr.toString())!!.index
                        method.instructions.add(IincInsnNode(index, 1))
                        this(node.expr)
                    }
                    PrefixOperator.DECR -> {
                        val index = node.table.lookup(node.expr.toString())!!.index
                        method.instructions.add(IincInsnNode(index, -1))
                        this(node.expr)
                    }
                    PrefixOperator.NOT -> {
                        TODO("handle not operations")
                    }
                }
            }
            is PostfixExpression -> {
                if (node.op is PostfixCallOperator) {
                    val ident = node.expr.toString()
                    // print is a special case
                    if (ident == "print") {
                        method.instructions.add(FieldInsnNode(
                                GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"
                        ))
                        val arg = node.op.args.first()
                        this(arg)
                        method.instructions.add(MethodInsnNode(
                                INVOKEVIRTUAL, "java/io/PrintStream", "print", "(${arg.type.type})V"
                        ))
                    } else {
                        this(node.op.args)
                        method.instructions.add(MethodInsnNode(
                                INVOKESTATIC, name, ident,
                                node.table.lookup(ident)!!.type.descriptor
                        ))
                    }
                } else {
                    this(node.expr)
                    method.instructions.add(IincInsnNode(
                            node.table.lookup(node.expr.toString())!!.index,
                            if (node.op == PostfixMathOperator.INCR) 1 else -1
                    ))
                }
            }
            is ComparisonExpression -> {
                this(node.left)
                this(node.right)
                if (node.boolean) {
                    if (node.float) {
                        method.instructions.add(InsnNode(FCMPG))
                        val jump = JumpInsnNode(when (node.op) {
                            ComparisonOperator.LT -> IFLT
                            ComparisonOperator.GT -> IFGT
                            ComparisonOperator.LTE -> IFLE
                            ComparisonOperator.GTE -> IFGE
                        }, null)
                        method.instructions.add(jump)
                        node.trueList = mutableListOf(jump)
                    } else {
                        val jump = JumpInsnNode(when (node.op) {
                            ComparisonOperator.LT -> IF_ICMPLT
                            ComparisonOperator.GT -> IF_ICMPGT
                            ComparisonOperator.LTE -> IF_ICMPLE
                            ComparisonOperator.GTE -> IF_ICMPGE
                        }, null)
                        method.instructions.add(jump)
                        node.trueList = mutableListOf(jump)
                    }
                    val jump = JumpInsnNode(GOTO, null)
                    method.instructions.add(jump)
                    node.falseList = mutableListOf(jump)
                } else {
                    if (node.float) {
                        method.instructions.add(InsnNode(FCMPG))
                        val jump = JumpInsnNode(when (node.op) {
                            ComparisonOperator.LT -> IFLT
                            ComparisonOperator.GT -> IFGT
                            ComparisonOperator.LTE -> IFLE
                            ComparisonOperator.GTE -> IFGE
                        }, null)
                        method.instructions.add(InsnNode(ICONST_0))
                        val end = LabelNode()
                        method.instructions.add(JumpInsnNode(GOTO, end))
                        jump.label = this.label
                        method.instructions.add(InsnNode(ICONST_1))
                        method.instructions.add(end)
                    } else {
                        val begin = LabelNode()
                        method.instructions.add(JumpInsnNode(when (node.op) {
                            ComparisonOperator.LT -> IF_ICMPLT
                            ComparisonOperator.GT -> IF_ICMPGT
                            ComparisonOperator.LTE -> IF_ICMPLE
                            ComparisonOperator.GTE -> IF_ICMPGE
                        }, begin))
                        method.instructions.add(InsnNode(ICONST_0))
                        val end = LabelNode()
                        method.instructions.add(JumpInsnNode(GOTO, end))
                        method.instructions.add(begin)
                        method.instructions.add(InsnNode(ICONST_1))
                        method.instructions.add(end)
                    }
                }
            }
            is DisjunctionExpression -> {
                TODO("handle disjunction expressions")
            }
            is ConjunctionExpression -> {
                TODO("handle conjunction expressions")
            }
            is EqualityExpression -> {
                this(node.left)
                this(node.right)
                if (node.boolean) {
                    var jump: JumpInsnNode
                    if (node.float) {
                        method.instructions.add(InsnNode(FCMPG))
                        jump = JumpInsnNode(when (node.op) {
                            EqualityOperator.EQ, EqualityOperator.ID_EQ -> IFNE
                            EqualityOperator.NEQ, EqualityOperator.ID_NEQ -> IFNE
                        }, null)
                    } else if (node.left.type.type.string) {
                        method.instructions.add(InsnNode(SWAP))
                        method.instructions.add(MethodInsnNode(
                                INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z"
                        ))
                        jump = JumpInsnNode(when (node.op) {
                            EqualityOperator.EQ, EqualityOperator.ID_EQ -> IFNE
                            EqualityOperator.NEQ, EqualityOperator.ID_NEQ -> IFEQ
                        }, null)
                        method.instructions.add(jump)
                    } else {
                        jump = JumpInsnNode(when (node.op) {
                            EqualityOperator.EQ, EqualityOperator.ID_EQ -> IF_ICMPEQ
                            EqualityOperator.NEQ, EqualityOperator.ID_NEQ -> IF_ICMPNE
                        }, null)
                        method.instructions.add(jump)
                    }
                    node.trueList = mutableListOf(jump)
                    jump = JumpInsnNode(GOTO, null)
                    method.instructions.add(jump)
                    node.falseList = mutableListOf(jump)
                } else {
                    if (node.left.type.type.string) {
                        val begin = LabelNode()
                        method.instructions.add(MethodInsnNode(
                                INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z"
                        ))
                        method.instructions.add(JumpInsnNode(when (node.op) {
                            EqualityOperator.EQ, EqualityOperator.ID_EQ -> IFNE
                            EqualityOperator.NEQ, EqualityOperator.ID_NEQ -> IFEQ
                        }, begin))
                        method.instructions.add(InsnNode(ICONST_0))
                        val end = LabelNode()
                        method.instructions.add(JumpInsnNode(GOTO, end))
                        method.instructions.add(begin)
                        method.instructions.add(InsnNode(ICONST_1))
                        method.instructions.add(end)
                    } else if (node.float) {
                        method.instructions.add(InsnNode(FCMPG))
                        val jump = JumpInsnNode(when (node.op) {
                            EqualityOperator.EQ, EqualityOperator.ID_EQ -> IFEQ
                            EqualityOperator.NEQ, EqualityOperator.ID_NEQ -> IFNE
                        }, null)
                        method.instructions.add(InsnNode(ICONST_0))
                        val end = LabelNode()
                        method.instructions.add(JumpInsnNode(GOTO, end))
                        jump.label = this.label
                        method.instructions.add(InsnNode(ICONST_1))
                        method.instructions.add(end)
                    } else {
                        val begin = LabelNode()
                        method.instructions.add(JumpInsnNode(when (node.op) {
                            EqualityOperator.EQ, EqualityOperator.ID_EQ -> IF_ICMPEQ
                            EqualityOperator.NEQ, EqualityOperator.ID_NEQ -> IF_ICMPNE
                        }, begin))
                        method.instructions.add(InsnNode(ICONST_0))
                        val end = LabelNode()
                        method.instructions.add(JumpInsnNode(GOTO, end))
                        method.instructions.add(begin)
                        method.instructions.add(InsnNode(ICONST_1))
                        method.instructions.add(end)
                    }
                }
            }
            is ContainingExpression -> {
                TODO("handle containing expressions")
            }
            is RangeExpression -> {
                TODO("handle range expressions")
            }
            is AdditiveExpression -> {
                this(node.left)
                this(node.right)
                method.instructions.add(when (node.op) {
                    AdditionOperator.ADD -> {
                        if (node.left.type.type.string) {
                            method.instructions.add(InsnNode(SWAP))
                            method.instructions.add(TypeInsnNode(NEW, "java/lang/StringBuilder"))
                            method.instructions.add(InsnNode(DUP))
                            method.instructions.add(MethodInsnNode(
                                    INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V"
                            ))
                            method.instructions.add(InsnNode(SWAP))
                            method.instructions.add(MethodInsnNode(
                                    INVOKEVIRTUAL, "java/lang/StringBuilder", "append",
                                    "(Ljava/lang/String;)Ljava/lang/StringBuilder;"
                            ))
                            method.instructions.add(InsnNode(SWAP))
                            method.instructions.add(MethodInsnNode(
                                    INVOKEVIRTUAL, "java/lang/StringBuilder", "append",
                                    "(${node.right.type.type})Ljava/lang/StringBuilder;"
                            ))
                            MethodInsnNode(
                                    INVOKEVIRTUAL, "java/lang/StringBuilder",
                                    "toString", "()Ljava/lang/String;"
                            )
                        } else {
                            InsnNode(node.type.type.getOpcode(IADD))
                        }
                    }
                    AdditionOperator.SUB -> InsnNode(node.type.type.getOpcode(ISUB))
                })
            }
            is MultiplicativeExpression -> {
                this(node.left)
                this(node.right)
                method.instructions.add(when (node.op) {
                    MultiplicationOperator.MUL -> InsnNode(node.type.type.getOpcode(IMUL))
                    MultiplicationOperator.DIV -> InsnNode(node.type.type.getOpcode(IDIV))
                    MultiplicationOperator.MOD -> InsnNode(node.type.type.getOpcode(IREM))
                })
            }
            is ParenExpression -> {
                this(node.expr)
            }
            is LiteralExpression<*> -> {
                if (node.boolean) {
                    val jump = JumpInsnNode(GOTO, null)
                    method.instructions.add(jump)
                    node.trueList.add(jump)
                } else {
                    if (node.value is Boolean) {
                        method.instructions.add(InsnNode(node.value.const))
                    } else {
                        method.instructions.add(LdcInsnNode(node.value))
                    }
                }
            }
            is IdentifierExpression -> {
                val info = node.table.lookup(node.toString())!!
                method.instructions.add(if (klass.fields.any { it.name == info.id }) {
                    FieldInsnNode(GETSTATIC, name, info.id, info.type.descriptor)
                } else {
                    VarInsnNode(info.type.getOpcode(ILOAD), info.index)
                })
            }
            is FunctionDeclaration -> {
                val lastMethod = method
                val desc = node.table.lookup(node.ident)!!.type.descriptor
                method = MethodNode(ACC_PUBLIC or ACC_STATIC, node.ident, desc, null, null)
                this(node.params)
                this(node.block)
                method.instructions.add(InsnNode(
                        (node as ASTNode).type.returnType.getOpcode(IRETURN)
                ))
                method.visitMaxs(0, 0)
                klass.methods.add(method)
                method = lastMethod
            }
            is PropertyDeclaration -> {
                val id = node.variable.ident
                if (method.name == "<clinit>") {
                    val final = if (node.kind == PropKind.VAL) ACC_FINAL else 0
                    val value = when (node.expression) {
                        null -> null
                        is LiteralExpression<*> -> node.expression.value
                        else -> this(node.expression).let { null }
                    }
                    val field = FieldNode(
                            ACC_PUBLIC or ACC_STATIC or final,
                            id, node.type.type.descriptor, null, value
                    )
                    if (value == null) {
                        method.instructions.add(FieldInsnNode(
                                PUTSTATIC, name, field.name, field.desc
                        ))
                    }
                    klass.fields.add(field)
                } else if (node.expression != null) {
                    this(node.expression)
                    method.instructions.add(VarInsnNode(
                            node.type.type.getOpcode(ISTORE), node.table.lookup(id)!!.index
                    ))
                }
            }
            is DoWhileStatement -> {
                node.expression.boolean = true
                val begin = this.label
                this(node.statement)
                node.nextList.addAll(node.statement.breakList)
                val expr = this.label
                backpatch(node.statement.nextList, expr)
                backpatch(node.statement.continueList, expr)
                this(node.expression)
                node.nextList.addAll(node.expression.falseList)
                backpatch(node.expression.trueList, begin)
            }
            is WhileStatement -> {
                node.expression.boolean = true
                val begin = this.label
                this(node.expression)
                val stmt = this.label
                backpatch(node.expression.trueList, stmt)
                this(node.statement)
                backpatch(node.statement.nextList, begin)
                backpatch(node.statement.continueList, begin)
                method.instructions.add(JumpInsnNode(GOTO, begin))
                node.nextList.addAll(node.expression.falseList)
                node.nextList.addAll(node.statement.breakList)
            }
            is ForStatement -> {
                this(node.expression.left)
                val (_, type, _, index) = node.table[node.variable.ident]!!
                method.instructions.add(VarInsnNode(type.getOpcode(ISTORE), index))
                val begin = this.label
                this(node.expression.right)
                method.instructions.add(when (node.expression.op) {
                    RangeOperator.DOWN_TO -> JumpInsnNode(IF_ICMPLT, begin)
                    RangeOperator.RANGE -> JumpInsnNode(IF_ICMPGT, begin)
                    RangeOperator.UNTIL -> JumpInsnNode(IF_ICMPGE, begin)
                    RangeOperator.STEP -> node.error("Unexpected operator step.")
                })
                method.instructions.add(VarInsnNode(type.getOpcode(ILOAD), index))
                if (node.expression.step == null || node.expression.step is LiteralExpression<*>) {
                    val value = (node.expression.step as? LiteralExpression<*>)?.value as? Int ?: 1
                    method.instructions.add(IincInsnNode(
                            index, if (node.expression.op == RangeOperator.DOWN_TO) -value else value
                    ))
                } else {
                    this(node.expression.step)
                    method.instructions.add(InsnNode(
                            if (node.expression.op == RangeOperator.DOWN_TO) ISUB else IADD
                    ))
                }
                val stmt = LabelNode()
                this(node.statement)
                method.instructions.add(JumpInsnNode(GOTO, begin))
                method.instructions.add(stmt)
                backpatch(node.statement.continueList, stmt)
                node.nextList.addAll(node.expression.falseList)
                node.nextList.addAll(node.statement.breakList)
            }
            is IfStatement -> {
                node.expression.boolean = true
                this(node.expression)
                val label = this.label
                this(node.statement)
                backpatch(node.expression.trueList, label)
                node.breakList.addAll(node.statement.breakList)
                node.continueList.addAll(node.statement.continueList)
                node.nextList.addAll(node.expression.falseList)
                node.nextList.addAll(node.statement.nextList)
            }
            is IfElseStatement -> {
                node.expression.boolean = true
                this(node.expression)
                val label1 = this.label
                this(node.statements.first)
                val goto = JumpInsnNode(GOTO, null)
                method.instructions.add(goto)
                val label2 = this.label
                this(node.statements.second)
                backpatch(node.expression.trueList, label1)
                backpatch(node.expression.falseList, label2)
                node.nextList.addAll(node.statements.first.nextList)
                node.nextList.addAll(node.statements.second.nextList)
                node.nextList.add(goto)
                node.breakList.addAll(node.statements.first.breakList)
                node.breakList.addAll(node.statements.second.breakList)
                node.continueList.addAll(node.statements.first.continueList)
                node.continueList.addAll(node.statements.second.continueList)
            }
            is AssignStatement -> {
                val (_, type, _, index) = node.table.lookup(node.ident)!!
                when (node.op) {
                    AssignmentOperator.ASSIGN -> {
                        this(node.expression)
                        method.instructions.add(VarInsnNode(type.getOpcode(ISTORE), index))
                    }
                    AssignmentOperator.ADD_ASSIGN -> {
                        if (type.string) {
                            this(node.expression)
                            method.instructions.add(TypeInsnNode(NEW, "java/lang/StringBuilder"))
                            method.instructions.add(InsnNode(DUP))
                            method.instructions.add(MethodInsnNode(
                                    INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V"
                            ))
                            method.instructions.add(VarInsnNode(type.getOpcode(ILOAD), index))
                            method.instructions.add(MethodInsnNode(
                                    INVOKEVIRTUAL, "java/lang/StringBuilder", "append",
                                    "(Ljava/lang/String;)Ljava/lang/StringBuilder;"
                            ))
                            method.instructions.add(InsnNode(SWAP))
                            method.instructions.add(MethodInsnNode(
                                    INVOKEVIRTUAL, "java/lang/StringBuilder", "append",
                                    "(Ljava/lang/String;)Ljava/lang/StringBuilder;"
                            ))
                            method.instructions.add(MethodInsnNode(
                                    INVOKEVIRTUAL, "java/lang/StringBuilder",
                                    "toString", "()Ljava/lang/String;"
                            ))
                        } else {
                            method.instructions.add(VarInsnNode(type.getOpcode(ILOAD), index))
                            this(node.expression)
                            method.instructions.add(InsnNode(type.getOpcode(IADD)))
                        }
                    }
                    AssignmentOperator.SUB_ASSIGN -> {
                        method.instructions.add(VarInsnNode(type.getOpcode(ILOAD), index))
                        this(node.expression)
                        method.instructions.add(InsnNode(type.getOpcode(ISUB)))
                    }
                    AssignmentOperator.MUL_ASSIGN -> {
                        method.instructions.add(VarInsnNode(type.getOpcode(ILOAD), index))
                        this(node.expression)
                        method.instructions.add(InsnNode(type.getOpcode(IMUL)))
                    }
                    AssignmentOperator.DIV_ASSIGN -> {
                        method.instructions.add(VarInsnNode(type.getOpcode(ILOAD), index))
                        this(node.expression)
                        method.instructions.add(InsnNode(type.getOpcode(IDIV)))
                    }
                    AssignmentOperator.MOD_ASSIGN -> {
                        method.instructions.add(VarInsnNode(type.getOpcode(ILOAD), index))
                        this(node.expression)
                        method.instructions.add(InsnNode(type.getOpcode(IREM)))
                    }
                }
            }
            is ReturnStatement -> {
                if (node.expression != null) this(node.expression)
            }
            is BreakStatement -> {
                val goto = JumpInsnNode(GOTO, null)
                method.instructions.add(goto)
                node.breakList.add(goto)
            }
            is ContinueStatement -> {
                val goto = JumpInsnNode(GOTO, null)
                method.instructions.add(goto)
                node.continueList.add(goto)
            }
            is ExpressionStatement -> {
                this(node.expression)
            }
            is BlockStatement -> node.iterator().let {
                val breaks = mutableListOf<JumpInsnNode>()
                val contns = mutableListOf<JumpInsnNode>()
                var stat: Statement? = null
                while (it.hasNext()) {
                    if (stat != null && stat.nextList.isNotEmpty())
                        backpatch(stat.nextList, this.label)
                    stat = it.next()
                    this(stat)
                    breaks += stat.breakList
                    contns += stat.continueList
                }
                if (stat != null && stat.nextList.isNotEmpty())
                    backpatch(stat.nextList, this.label)
                stat?.run { node.nextList = nextList }
                node.breakList = breaks
                node.continueList = contns
            }
            is Arguments -> {
                for (a in node) this(a)
            }
            is Argument -> {
                this(node.expression)
            }
            is Variable -> {
                // NOOP
            }
            is FunctionParameters -> {
                node.forEachIndexed { idx, param ->
                    method.instructions.add(VarInsnNode(param.type.type.getOpcode(ILOAD), idx))
                }
            }
            is Parameter -> {
                // NOOP
            }
        }
        return writer
    }

    /** Used to convert the boolean to an `ICONST` opcode. */
    private inline val Boolean.const get() = if (this) ICONST_1 else ICONST_0

    /** Used to check whether either node of a binary expression is a float. */
    private inline val BinaryExpression<*>.float get() = left.type.type.float || right.type.type.float

    companion object {
        /**
         * Backpatches a list of instructions with the given target.
         *
         * @param list a list of jump instructions.
         * @param target the target label to go to.
         */
        @JvmStatic fun backpatch(list: List<JumpInsnNode>?, target: LabelNode) = list?.forEach { it.label = target }

        /**
         * Creates and invokes a new instance, runs [TraceClassVisitor], and returns the bytecode.
         *
         * @param name the name of the class.
         * @param node the AST node of the class.
         */
        @JvmName("trace") @JvmStatic
        operator fun invoke(name: String, node: ASTNode) = with(BytecodeGenerator(name)) {
            val writer = this(node)
            val bytes = writer.toByteArray()
            klass.accept(TraceClassVisitor(writer, java.io.PrintWriter(System.out)))
            return@with bytes!!
        }
    }
}
