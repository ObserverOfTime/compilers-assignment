package gr.hua.dit.it21685.compilers.bytecode

import gr.hua.dit.it21685.compilers.ast.*

/** An AST visitor that builds a local index pool. */
class LocalIndexBuilder : ASTVisitor<LocalIndexStack> {
    /** A stack of local index pools. */
    private val stack = LocalIndexStack()

    @Throws(ASTException::class)
    override fun invoke(node: ASTNode): LocalIndexStack {
        when (node) {
            is CompilationUnit -> stack.scope {
                node.pool = it
                for (d in node) this(d)
            }
            is PrefixExpression -> {
                node.pool = stack.element()
                this(node.expr)
            }
            is PostfixExpression -> {
                node.pool = stack.element()
                if (node.op is PostfixCallOperator) this(node.op.args)
                this(node.expr)
            }
            is ComparisonExpression -> {
                node.pool = stack.element()
                this(node.left)
                this(node.right)
            }
            is DisjunctionExpression -> {
                node.pool = stack.element()
                this(node.left)
                this(node.right)
            }
            is ConjunctionExpression -> {
                node.pool = stack.element()
                this(node.left)
                this(node.right)
            }
            is EqualityExpression -> {
                node.pool = stack.element()
                this(node.left)
                this(node.right)
            }
            is ContainingExpression -> {
                node.pool = stack.element()
                this(node.left)
                this(node.right)
            }
            is RangeExpression -> {
                node.pool = stack.element()
                this(node.left)
                this(node.right)
                if (node.step != null) this(node.step)
            }
            is AdditiveExpression -> {
                node.pool = stack.element()
                this(node.left)
                this(node.right)
            }
            is MultiplicativeExpression -> {
                node.pool = stack.element()
                this(node.left)
                this(node.right)
            }
            is ParenExpression -> {
                node.pool = stack.element()
                this(node.expr)
            }
            is LiteralExpression<*> -> {
                node.pool = stack.element()
            }
            is IdentifierExpression -> {
                node.pool = stack.element()
            }
            is FunctionDeclaration -> stack.scope {
                node.pool = it
                this(node.params)
                this(node.block)
            }
            is PropertyDeclaration -> stack.element().let {
                node.pool = it
                this(node.variable)
                if (node.expression != null) this(node.expression)
                node.table[node.variable.ident]!!.index = it.acquireIndex()
            }
            is DoWhileStatement -> {
                node.pool = stack.element()
                this(node.statement)
                this(node.expression)
            }
            is WhileStatement -> {
                node.pool = stack.element()
                this(node.expression)
                this(node.statement)
            }
            is ForStatement -> stack.element().let {
                node.pool = it
                this(node.expression)
                this(node.statement)
                node.table[node.variable.ident]!!.index = it.acquireIndex()
            }
            is IfStatement -> {
                node.pool = stack.element()
                this(node.expression)
                this(node.statement)
            }
            is IfElseStatement -> {
                node.pool = stack.element()
                this(node.expression)
                this(node.statements.first)
                this(node.statements.second)
            }
            is AssignStatement -> {
                node.pool = stack.element()
                this(node.expression)
            }
            is ReturnStatement -> {
                node.pool = stack.element()
                if (node.expression != null) this(node.expression)
            }
            is BreakStatement -> {
                node.pool = stack.element()
            }
            is ContinueStatement -> {
                node.pool = stack.element()
            }
            is ExpressionStatement -> {
                node.pool = stack.element()
            }
            is BlockStatement -> stack.scope {
                node.pool = it
                for (s in node) this(s)
            }
            is Arguments -> {
                node.pool = stack.element()
                for (a in node) this(a)
            }
            is Argument -> {
                node.pool = stack.element()
                this(node.expression)
            }
            is Variable -> {
                node.pool = stack.element()
            }
            is FunctionParameters -> {
                node.pool = stack.element()
                for (p in node) this(p)
            }
            is Parameter -> stack.element().let {
                node.pool = it
                node.table[node.ident]!!.index = it.acquireIndex()
            }
        }
        return stack
    }

    companion object {
        /**
         * Creates and invokes a new instance.
         *
         * @param node an AST node object.
         * @return the local index pool stack of the instance.
         */
        @JvmSynthetic
        operator fun invoke(node: ASTNode) = LocalIndexBuilder()(node)
    }
}


