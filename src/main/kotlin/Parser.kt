package gr.hua.dit.it21685.compilers

import gr.hua.dit.it21685.compilers.ast.ASTNode

/**
 * Grammar parser implementation.
 *
 * @property input The input reader object.
 */
@Suppress("DEPRECATION")
class Parser(private val input: java.io.Reader) : BaseParser(Lexer(input)), AutoCloseable {
    /** The line of the current token. */
    private val line get() = cur_token?.left ?: 0

    /** The column of the current token. */
    private val column get() = cur_token?.right ?: 0

    override fun report_error(message: String?, info: Any?) = logger.error {
        "($line:$column) $message" + (info?.let { " @ $it" } ?: "")
    }

    override fun debug_message(message: String?) = logger.debug { message }

    /**
     * Returns the current token cast to a node type.
     *
     * @param T the type of the node.
     * @throws [ClassCastException] when casting to the wrong node type.
     * @throws [Exception] if an error occurred while parsing the file.
     */
    @Throws(ClassCastException::class, Exception::class)
    internal inline fun <reified T : ASTNode> node() = (cur_token ?: debug_parse()).value as T

    @Throws(java.io.IOException::class)
    override fun close() = input.close()

    companion object : mu.KLogging()
}
