import gr.hua.dit.it21685.compilers.Parser
import gr.hua.dit.it21685.compilers.ast.CompilationUnit
import gr.hua.dit.it21685.compilers.bytecode.BytecodeGenerator
import gr.hua.dit.it21685.compilers.bytecode.LocalIndexBuilder
import gr.hua.dit.it21685.compilers.bytecode.ReloadingClassLoader
import gr.hua.dit.it21685.compilers.symbol.SymbolTableBuilder
import gr.hua.dit.it21685.compilers.type.TypeCollector
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.blocking.forAll
import io.kotest.matchers.shouldBe
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.io.StringReader

class BytecodeTest : StringSpec({
    val stdout: PrintStream = System.out

    val out = ByteArrayOutputStream()

    fun Parser.parse(name: String, body: String) = use {
        val node = it.node<CompilationUnit>()
        SymbolTableBuilder(node)
        TypeCollector(node)
        LocalIndexBuilder(node)
        ReloadingClassLoader().apply {
            this[name] = BytecodeGenerator(name)(node).toByteArray()
        }[name].getMethod("main", Array<String>::class.java)(null, null)
        out.toString() shouldBe body
    }

    beforeTest {
        System.setOut(PrintStream(out))
    }

    afterTest {
        out.reset()
        System.setOut(stdout)
    }

    "can generate working bytecode for hello world" {
        Parser(StringReader("""
                val hello = "Hello ";

                fun world(): String { return "world!"; }

                fun main() { print(hello + world() + '\n'); }
            """.trimIndent())).parse("Hello", "Hello world!\n")
    }

    "can generate working bytecode for all samples".config(enabled = false) {
        forAll(
                "sample1.kt" to (0..99).joinToString("\n") + "\nDone\n",
                "sample2.kt" to "Sum is ${(0..100).sum()}\n",
                "sample3.kt" to "OK\nSum is ${(0..100).sum()}\n"
        ) { name, body ->
            Parser(resourceReader(name)).parse(name.substringBeforeLast('.'), body + '\n')
        }
    }
})
