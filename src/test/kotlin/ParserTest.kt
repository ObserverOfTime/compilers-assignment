import gr.hua.dit.it21685.compilers.Parser
import gr.hua.dit.it21685.compilers.ast.CompilationUnit
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.matchers.shouldBe

class ParserTest : StringSpec({
    "can parse all samples" {
        forAll(
                "sample1.kt" to """
                    fun main() {
                    var i = 0;
                    while (i < 100) {
                    print(i++);
                    print('\n');
                    }
                    if (i == 100) {
                    print("Done\n");
                    }
                    }
                """,
                "sample2.kt" to """
                    fun sum2(x: Int): Int {
                    if (x <= 0) {
                    return 0;
                    }
                    return x + sum2(x - 1);
                    }
                    fun main() {
                    var total: Int;
                    total = sum2(100);
                    print("Sum is " + total + '\n');
                    }
                """,
                "sample3.kt" to """
                    fun sum3(x: Int): Int {
                    var res = 0;
                    for (i in 1..x) res = res + i;
                    return res;
                    }
                    fun main() {
                    val x = 10;
                    val limit = 100;
                    if (x in 1 until limit) print("OK\n");
                    var total: Int;
                    total = sum3(limit);
                    print("Sum is " + total + '\n');
                    }
                """
        ) { name, body ->
            Parser(resourceReader(name)).use {
                it.node<CompilationUnit>().toString() shouldBe body
            }
        }
    }
})
