import gr.hua.dit.it21685.compilers.Parser
import gr.hua.dit.it21685.compilers.ast.CompilationUnit
import gr.hua.dit.it21685.compilers.ir.IRGenerator
import gr.hua.dit.it21685.compilers.symbol.SymbolTableBuilder
import gr.hua.dit.it21685.compilers.type.TypeCollector
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.matchers.shouldBe

class IRTest : StringSpec({
    "can generate IR code for all samples" {
        forAll(
                "sample1.kt" to """
                    -- main --
                    i = 0
                    ${'$'}L1:
                    ${'$'}t0 = 100
                    if i < ${'$'}t0 goto ${'$'}L2
                    goto ${'$'}L3
                    ${'$'}L2:
                    ${'$'}t1 = i++
                    param ${'$'}t1
                    invoke print, 1
                    ${'$'}t2 = '\n'
                    param ${'$'}t2
                    invoke print, 1
                    goto ${'$'}L1
                    ${'$'}L3:
                    ${'$'}t3 = 100
                    if i == ${'$'}t3 goto ${'$'}L4
                    goto ${'$'}L5
                    ${'$'}L4:
                    ${'$'}t4 = "Done\n"
                    param ${'$'}t4
                    invoke print, 1
                    ${'$'}L5:
                """,
                "sample2.kt" to """
                    -- sum2 --
                    ${'$'}t0 = 0
                    if x <= ${'$'}t0 goto ${'$'}L1
                    goto ${'$'}L2
                    ${'$'}L1:
                    ${'$'}t1 = 0
                    return ${'$'}t1
                    ${'$'}L2:
                    ${'$'}t2 = 1
                    ${'$'}t3 = x - ${'$'}t2
                    param ${'$'}t3
                    ${'$'}t4 = invoke sum2, 1
                    ${'$'}t5 = x + ${'$'}t4
                    return ${'$'}t5

                    -- main --
                    ${'$'}t6 = 100
                    param ${'$'}t6
                    ${'$'}t7 = invoke sum2, 1
                    total = ${'$'}t7
                    ${'$'}t8 = "Sum is "
                    ${'$'}t9 = ${'$'}t8 + total
                    ${'$'}t10 = '\n'
                    ${'$'}t11 = ${'$'}t9 + ${'$'}t10
                    param ${'$'}t11
                    invoke print, 1
                """,
                "sample3.kt" to """
                    -- sum3 --
                    res = 0
                    ${'$'}t0 = 1
                    ${'$'}t1 = ${'$'}t0 .. x
                    ${'$'}L1:
                    unless i in ${'$'}t1 goto ${'$'}L3
                    ${'$'}t2 = res + i
                    res = ${'$'}t2
                    ${'$'}L2:
                    i += 1
                    goto ${'$'}L1
                    ${'$'}L3:
                    return res

                    -- main --
                    x = 10
                    limit = 100
                    ${'$'}t3 = 1
                    ${'$'}t4 = ${'$'}t3 until limit
                    if x in ${'$'}t4 goto ${'$'}L4
                    goto ${'$'}L5
                    ${'$'}L4:
                    ${'$'}t5 = "OK\n"
                    param ${'$'}t5
                    invoke print, 1
                    ${'$'}L5:
                    param limit
                    ${'$'}t6 = invoke sum3, 1
                    total = ${'$'}t6
                    ${'$'}t7 = "Sum is "
                    ${'$'}t8 = ${'$'}t7 + total
                    ${'$'}t9 = '\n'
                    ${'$'}t10 = ${'$'}t8 + ${'$'}t9
                    param ${'$'}t10
                    invoke print, 1
                """
        ) { name, body ->
            Parser(resourceReader(name)).use {
                val node = it.node<CompilationUnit>()
                SymbolTableBuilder(node)
                TypeCollector(node)
                IRGenerator(node).toString() shouldBe body
            }
        }
    }
})
