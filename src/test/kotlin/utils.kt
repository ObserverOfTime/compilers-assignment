import io.kotest.core.test.TestContext
import io.kotest.data.row

fun <T : TestContext> T.resourceReader(name: String) =
        this::class.java.getResourceAsStream("/$name").reader()

infix fun String.to(that: String) = row(this, that.trimIndent())
