/*
 * Sample program 1
 */
fun main() {
    var i = 0;
    while (i < 100) {
        print(i++);
        print('\n');
    }
    if (i == 100) {
        print("Done\n");
    }
}
