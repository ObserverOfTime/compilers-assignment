/*
 * Sample program 3
 */
fun sum3(x: Int): Int {
    var res = 0;
    for (i in 1..x)
        res = res + i;
    return res;
}

fun main() { // main function
    val x = 10;
    val limit = 100;
    if (x in 1 until limit)
        print("OK\n");
    var total: Int;
    total = sum3(limit);
    print("Sum is " + total + '\n');
}
