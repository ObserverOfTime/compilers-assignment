/*
 * Sample program 2
 */
fun sum2(x: Int): Int {
    if (x <= 0) {
        return 0;
    }
    return x + sum2(x - 1);
}

fun main() { // main function
    var total: Int;
    total = sum2(100);
    print("Sum is " + total + '\n');
}
