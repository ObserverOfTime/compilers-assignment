## VerySimpleKotlin

```sh
# build the package
mvn clean package

# print the parsed code
java -jar \
    target/VerySimpleKotlin-*-jar-with-dependencies.jar \
    src/test/resources/*.kt -v -r text

# print the intermediate code
java -jar \
    target/VerySimpleKotlin-*-jar-with-dependencies.jar \
    src/test/resources/*.kt -v -r ir

# save the class files
java -jar \
    target/VerySimpleKotlin-*-jar-with-dependencies.jar \
    src/test/resources/*.kt -v -r bytecode -o target

# execute the code
java -jar \
    target/VerySimpleKotlin-*-jar-with-dependencies.jar \
    src/test/resources/*.kt -v -r bytecode
```
